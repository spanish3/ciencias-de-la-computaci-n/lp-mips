`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:12:30
// Design Name: 
// Module Name: Eq_Comparator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Eq_Comparator
#(
    parameter   NB_DATA  = 32
 )
 (
    // Entradas
    input   wire    [NB_DATA   - 1 : 0]    i_Data_1,
    input   wire    [NB_DATA   - 1 : 0]    i_Data_2,
    
    // Salidas
    output  wire                           o_Equal
 );
 
    reg     aux;
    
    assign o_Equal = aux;
    
    always@(*)
    begin
        if(i_Data_1 == i_Data_2)
        begin
            aux = 1;
        end
        else
        begin
            aux = 0;
        end
    end
 
endmodule
