`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:13:36
// Design Name: 
// Module Name: ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU
#(
    parameter   NB_DATA  = 32,
    parameter   NB_ALUOP = 4,
    parameter   NB_SHAMT = 5
 )
 (
    // Entradas
    input   wire    [NB_DATA   - 1 : 0]    i_Data_1,
    input   wire    [NB_DATA   - 1 : 0]    i_Data_2,
    input   wire    [NB_ALUOP  - 1 : 0]    i_Op,
    input   wire    [NB_SHAMT  - 1 : 0]    i_Shamt,
    
    // Salidas
    output  wire                           o_Zero,
    output  wire    [NB_DATA  - 1 : 0]     o_ALU_Result
 );
 
localparam SLL  = 4'b0000;  // 0
localparam SRL  = 4'b0001;  // 1
localparam SRA  = 4'b0010;  // 2
localparam SLLV = 4'b0011;  // 3
localparam SRLV = 4'b0100;  // 4
localparam SRAV = 4'b0101;  // 5
localparam ADDU = 4'b0110;  // 6
localparam SUBU = 4'b0111;  // 7
localparam AND  = 4'b1000;  // 8
localparam OR   = 4'b1001;  // 9
localparam XOR  = 4'b1010;  // 10
localparam NOR  = 4'b1011;  // 11
localparam SLT  = 4'b1100;  // 12
localparam JALR = 4'b1101;  // 13
localparam LUI  = 4'b1110;  // 14

reg    [NB_DATA - 1: 0]   ALU_Result;

assign o_ALU_Result = ALU_Result;
assign o_Zero = ALU_Result == 0 ? 0 : 1;
 
 always @(*)
 begin
    case(i_Op)
        SLL  : ALU_Result =   i_Data_2 <<  i_Shamt;
        SRL  : ALU_Result =   i_Data_2 >>  i_Shamt;
        SRA  : ALU_Result =   $signed(i_Data_2) >>> $signed(i_Shamt);
        SLLV : ALU_Result =   i_Data_2 <<  i_Data_1[4:0];
        SRLV : ALU_Result =   i_Data_2 >>  i_Data_1[4:0];
        SRAV : ALU_Result =   $signed(i_Data_2) >>> $signed(i_Data_1[4:0]);
        ADDU : ALU_Result =   i_Data_1  +  i_Data_2;
        SUBU : ALU_Result =   i_Data_1  -  i_Data_2;
        AND  : ALU_Result =   i_Data_1  &  i_Data_2;
        OR   : ALU_Result =   i_Data_1  |  i_Data_2;
        XOR  : ALU_Result =   i_Data_1  ^  i_Data_2;
        NOR  : ALU_Result = ~(i_Data_1  |  i_Data_2);
        SLT  : ALU_Result =   i_Data_1  <  i_Data_2 ? 32'b1 : 32'b0;
        JALR : ALU_Result =   i_Data_1  +  1'b1;
        LUI  : ALU_Result =   i_Data_2 <<  16;
        
        default:   ALU_Result = {NB_DATA{1'b0}}; 
    endcase
 end
 
endmodule
