`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:07:49
// Design Name: 
// Module Name: PC
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PC
#(
    parameter   NB_DIR_PC = 32
 )
 (
    // Entradas
    input   wire                       i_clk,
	input   wire                       i_Reset,
	input   wire                       i_Stall,
	input   wire                       i_Halt,     // Detener
	input   wire [NB_DIR_PC - 1 : 0]   i_PC_In,  // Pr?xima dir de la memoria de programa
	
	// Salidas
	output  wire [NB_DIR_PC - 1 : 0]   o_PC_Out   // La salida de PC indica la direcci?n a leer de la memoria de programa    
 );
 
 // Registros Internos
 reg    [NB_DIR_PC - 1 : 0]   ProgCounter;
 
 	always @(posedge i_clk)
 	begin
		if (i_Reset)
		begin
		      ProgCounter <= {NB_DIR_PC{1'b0}};
		      //$display("PC Reset");
		end 
		else if (i_Halt || i_Stall)
			ProgCounter <= ProgCounter;
		else
		begin
			ProgCounter <= i_PC_In;		
			//$display("Se escribe PC con %d",i_PC_In);
        end           
	end

	assign o_PC_Out   = ProgCounter;
 
endmodule

