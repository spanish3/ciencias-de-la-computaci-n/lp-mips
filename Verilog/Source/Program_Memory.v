`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:08:26
// Design Name: 
// Module Name: Program_Memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Program_Memory
#(
    parameter   NB_DATA   = 32,
    parameter   NB_UART   = 8
 )
(
    // Entradas
    input   wire                        i_clk,                  // Clock
    input   wire                        i_Reset,                // Reset
    input   wire                        i_Prog_Mem_Write, 
    input   wire    [NB_DATA - 1 : 0]   i_Address,
    input   wire    [NB_DATA - 1 : 0]   i_Write_Address,
    input   wire    [NB_UART - 1 : 0]   i_Write_Data,
    
    //Salidas
    output  wire    [NB_DATA - 1 : 0]   o_Instruction
 );  
 
 // Registros internos
 	reg [NB_DATA - 1 : 0] Memory[0:1023];    // Memoria propiamente dicha
	reg [NB_DATA - 1 : 0] tmp_Read           ;    // Registro para output
	
initial
begin
    $readmemb("C:\\Users\\Lucas\\Desktop\\Arqui\\Code\\TPF-RC12\\Code.mem",Memory);
end

//Leer
always @(negedge i_clk) 
begin 
    if(i_Reset) 
    begin
        tmp_Read <= {6'b111110, 26'b0};       
    end 
    else 
    begin
        if(!i_Prog_Mem_Write)
        begin
            tmp_Read <= Memory[i_Address>>2];
        end        
    end
end

//Escribir
always @(posedge i_clk) 
begin 
    if(i_Reset) 
        begin
            //	<= 0;
        end 
    else 
        begin
            if(i_Prog_Mem_Write)
            begin 
                case (i_Write_Address[1:0])
                    0:
                    begin
                        Memory[i_Write_Address>>2] <= {Memory[i_Write_Address>>2][31:8],i_Write_Data};
                        //$display("Se escribe prog_Mem[%d] con %d",i_Write_Address,i_Write_Data[7:0]);
                    end
                    1:
                    begin
                        Memory[i_Write_Address>>2] <= {Memory[i_Write_Address>>2][31:16],i_Write_Data,Memory[i_Write_Address>>2][7:0]};
                        //$display("Se escribe prog_Mem[%d] con %d",i_Write_Address,i_Write_Data[7:0]);
                    end
                    2:
                    begin
                        Memory[i_Write_Address>>2] <= {Memory[i_Write_Address>>2][31:24],i_Write_Data,Memory[i_Write_Address>>2][15:0]};
                        //$display("Se escribe prog_Mem[%d] con %d",i_Write_Address,i_Write_Data[7:0]);
                    end
                    3:
                    begin
                        Memory[i_Write_Address>>2] <= {i_Write_Data,Memory[i_Write_Address>>2][23:0]};
                        //$display("Se escribe prog_Mem[%d] con %d",i_Write_Address,i_Write_Data[7:0]);
                    end
                endcase
            end
     end
end

assign o_Instruction = tmp_Read;
 
endmodule
