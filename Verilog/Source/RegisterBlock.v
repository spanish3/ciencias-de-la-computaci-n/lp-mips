`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:10:32
// Design Name: 
// Module Name: RegisterBlock
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RegisterBlock
#(
    parameter   NB_DATA = 32,
    parameter   NB_RDIR = 5,
    parameter   NB_ALLR = 160 
 )
(
    // Entradas
    input   wire                        i_clk,                  // Clock
    input   wire                        i_Reset,                // Reset
    input   wire    [NB_RDIR - 1 : 0]   i_Src_Register_Out_1,   // Direccionamiento para la salida 1
    input   wire    [NB_RDIR - 1 : 0]   i_Src_Register_Out_2,   // Direccionamiento para la salida 2
    input   wire    [NB_RDIR - 1 : 0]   i_Dest_Register,        // Direccionamiento para la escritura
    input   wire    [NB_DATA - 1 : 0]   i_Data_In,              // Datos para la escritura
    input   wire                        i_Write_Reg,            // Se?al de escritura
    
    // Salidas
    output  wire    [NB_DATA - 1 : 0]   o_Read_Data_1,
    output  wire    [NB_DATA - 1 : 0]   o_Read_Data_2,
    output  wire    [NB_ALLR - 1 : 0]   o_All_Registers
 );
 
 // Registros Internos
 reg   [NB_DATA - 1 : 0]   Registers [0 : NB_DATA - 1];    // Registros propiamente dichos
 reg   [NB_DATA - 1 : 0]   Read_Data_1;
 reg   [NB_DATA - 1 : 0]   Read_Data_2;
 
genvar i;        
generate 
    for(i = 0; i < 4 ; i = i +1)
    begin
        assign o_All_Registers[NB_DATA - 1 + NB_DATA * i : 0 + NB_DATA * i] = Registers[i];
    end
endgenerate
 
assign o_All_Registers[NB_DATA - 1 + NB_DATA * 4 : 0 + NB_DATA * 4] = Registers[31];
 
assign    o_Read_Data_1 = Read_Data_1;
assign    o_Read_Data_2 = Read_Data_2;
 
 // Leer registros / Reset
always @(negedge i_clk) 
     begin
        if(i_Reset)
            begin
                Read_Data_1 <= {NB_DATA{1'b0}};
                Read_Data_2 <= {NB_DATA{1'b0}};
            end
        else
            begin
                Read_Data_1 <= Registers[i_Src_Register_Out_1];
                Read_Data_2 <= Registers[i_Src_Register_Out_2];
                //$display("Se lee registro %d con %d",i_Src_Register_Out_1,Registers[i_Src_Register_Out_1]);
                //$display("Se lee registro %d con %d",i_Src_Register_Out_2,Registers[i_Src_Register_Out_2]);
            end    
     end
 
 // Escritura
 always @(posedge i_clk)
     begin
        if(i_Write_Reg && (!(i_Reset)))
            begin
                Registers[i_Dest_Register] <= i_Data_In;
                $display("Se escribe registro %d con %d",i_Dest_Register,i_Data_In);
            end
     end

endmodule
