#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "labelList.h"

#define TAM 1024

void addLabel(struct labelNode * head, char *text, int labelDir)
{ 
  if(!labelExists(head,text))
  {      
    struct labelNode * current = head;
    if(head->dir != -1)
    {
      // Ya hay elementos en la lista, agregar otro
      while (current->next != NULL) 
      {
        current = current->next;
      }
      current->next = (struct labelNode *) malloc(sizeof(struct labelNode));
      strcpy(current->next->labelText,text);
      current->next->dir = labelDir;
      current->next->next = NULL;
    }
    else
    {
      // Primer elemento de la lista
      strcpy(current->labelText,text);
      current->dir = labelDir;
      current->next = NULL;
    }
  }
}

int labelExists(struct labelNode * head, char *text)
{
  struct labelNode * current = head;
  while (current != NULL) 
  {
      if(!strcmp(current->labelText,text))
      {
        return 1;
      }
      current = current->next;
  }
  return 0;
}

void printLabelList(struct labelNode * head)
{
  struct labelNode * current = head;
  while (current != NULL) 
  {
      printf("dir: %d ; text: %s\n",current->dir,current->labelText);
      current = current->next;
  }
  //printf("La lista tiene %d nodos\n",LabelListSize(head));
}

int LabelListSize(struct labelNode * head)
{
  int count = 0;  // Initialize count
  struct labelNode * current = head;  // Initialize current
  while (current != NULL)
  {
      count++;
      current = current->next;
  }
  return count;
}

void freeList(struct labelNode * head)
{
   struct labelNode * tmp;

   while (head != NULL)
    {
       tmp = head;
       head = head->next;
       free(tmp);
    }
}

void *getLabelNumber(struct labelNode * head, char *label, char *dest)
{
  struct labelNode * current = head;
  if(labelExists(head,label))
  {
    while (current != NULL) 
    {
        if(!strcmp(current->labelText,label))
        {
          sprintf(dest,"%d",current->dir);
        }
        current = current->next;
    }
  }
  else
  {
    printf("label de salto inexistente\n");
    exit(EXIT_FAILURE);
  }
}

int getIntLabelNumber(struct labelNode * head, char *label)
{
  struct labelNode * current = head;
  if(labelExists(head,label))
  {
    while (current != NULL) 
    {
        if(!strcmp(current->labelText,label))
        {
          return current->dir;
        }
        current = current->next;
    }
  }
  else
  {
    printf("label de salto inexistente\n");
    return -1;
  }
}