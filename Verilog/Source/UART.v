`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:15:41
// Design Name: 
// Module Name: UART
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module UART
#( 
    parameter   NB_UART = 8,    // Cantidad de bits de datos
    parameter   SB_TICK = 16, 
    parameter   NB_CONT = 4,
    parameter   NB_BRCV = 3,
    parameter   M = 14        // Param pra Baud Rate Generator
 )
(
    input   wire                                    i_clk, i_Reset,
    input   wire                                    i_rx,
    input   wire              [NB_UART  - 1 : 0]    i_tx_data,
    input   wire                                    i_tx_start,
    
    output  wire                                    o_tx,
    output  wire                                    o_rx_done_tick,
    output  wire                                    o_tx_done_tick,
    output  wire              [NB_UART  - 1 : 0]    o_rx_data
);
  
    // Interno
    wire                                tick;
    
    
Baud_Rate_Generator
#(
    .M                  (M)
 )
u_Baud_rate_Gen_1
 (
    // Entradas
    .i_clk              (i_clk),
    .i_Reset            (i_Reset),

    // Salidas
    .s_tick             (tick)
 );

UART_RX
#(
    .NB_UART            (NB_UART),    // Cantidad de bits de datos
    .SB_TICK            (SB_TICK),   // Cantidad de ticks necesarios para los bits de stop
    .NB_STAT            (2),    // Cantidad de bits para representar los estdos    
    .NB_CONT            (NB_CONT),
    .NB_BRCV            (NB_BRCV)
 )
u_UART_RX_1
(
    // Entradas
    .i_clk              (i_clk),
    .i_Reset            (i_Reset),        
    .i_rx               (i_rx),           // rx
    .s_tick             (tick),         // Se?al de Baud Rate Gen
    
    // Salidas
    .rx_done_tick       (o_rx_done_tick),   // Indica que el dato ha sido recibido
    .dout               (o_rx_data)
);

UART_TX
#(
    .NB_UART            (NB_UART),    // Cantidad de bits de datos
    .SB_TICK            (SB_TICK),   // Cantidad de ticks necesarios para los bits de stop
    .NB_STAT            (2),    // Cantidad de bits para representar los estdos    
    .NB_CONT            (NB_CONT),
    .NB_BRCV            (NB_BRCV)
 )
u_UART_TX_1
 (
    // Entradas
    .i_clk              (i_clk),
    .i_Reset            (i_Reset),        
    .i_tx_start         (i_tx_start),           
    .s_tick             (tick),         // Se?al de Baud Rate Gen
    .i_din              (i_tx_data),
    
    // Salidas
    .o_tx_done_tick     (o_tx_done_tick),
    .o_tx               (o_tx)
 );
    
endmodule
