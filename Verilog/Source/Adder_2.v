`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:11:31
// Design Name: 
// Module Name: Adder_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Adder_2
#(
    parameter   NB_DATA = 32
 )
 (
    // Entradas
    input   wire    [NB_DATA - 1 : 0]    i_Data_1,
    input   wire    [NB_DATA - 1 : 0]    i_Data_2,
    // Salidas
    output  wire    [NB_DATA - 1 : 0]    o_AD2_Result
 ); 

assign o_AD2_Result =   i_Data_1  +  i_Data_2;
 
endmodule
