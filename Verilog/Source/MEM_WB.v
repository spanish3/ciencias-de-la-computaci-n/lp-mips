`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:14:20
// Design Name: 
// Module Name: MEM_WB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MEM_WB
#(
    parameter   NB_DATA  = 32,
    parameter   NB_RDIR  = 5,
    parameter   NB_ALUOP = 4,
    parameter   NB_MUX4  = 2
 )
(
    // clk y reset
    input   wire                         i_clk,                  // Clock
    input   wire                         i_Reset,                // Reset
    
    // Guardar datos de la etapa EX para utilizarlos en la etapa MEM
    input   wire    [NB_DATA  - 1 : 0]   i_ALU_Result_MEM,       // Resultado de la ALU
    input   wire    [NB_DATA  - 1 : 0]   i_Mem_Data_Out_MEM,  // Para escribir en memoria desde el banco de registros
    input   wire    [NB_DATA  - 1 : 0]   i_PC_Plus_Four_MEM,    // PC + 4
    
    input   wire    [NB_RDIR  - 1 : 0]   i_rs_MEM,
    input   wire    [NB_RDIR  - 1 : 0]   i_rt_MEM,
    input   wire    [NB_RDIR  - 1 : 0]   i_rd_MEM,
    input   wire    [NB_RDIR  - 1 : 0]   i_Dest_Register_MEM,    // Registro de escritura
    
    // Se?ales de control que deben pasarse de la etapa EX a la etapa WB
    input   wire    [NB_MUX4  - 1 : 0]   i_Mem_To_Reg_MEM,           // Selecciona el dato que pasa a escribirse en el registro
    input   wire                         i_Write_Reg_MEM,            // Se?al para escribir registros 
    
    //Salidas
        // Guardar datos de la etapa EX para utilizarlos en la etapa EX
    output  wire    [NB_DATA  - 1 : 0]   o_ALU_Result_WB,       // Resultado de la ALU
    output  wire    [NB_DATA  - 1 : 0]   o_Mem_Data_Out_WB,
    output  wire    [NB_DATA  - 1 : 0]   o_PC_Plus_Four_WB,
    
    output  wire    [NB_RDIR  - 1 : 0]   o_rs_WB,
    output  wire    [NB_RDIR  - 1 : 0]   o_rt_WB,
    output  wire    [NB_RDIR  - 1 : 0]   o_rd_WB,
    output  wire    [NB_RDIR  - 1 : 0]   o_Dest_Register_WB,    // Registro de escritura
    
    // Se?ales de control que deben pasarse de la etapa EX a la etapa WB
    output  wire    [NB_MUX4  - 1 : 0]   o_Mem_To_Reg_WB,           // Selecciona el dato que pasa a escribirse en el registro
    output  wire                         o_Write_Reg_WB            // Se?al para escribir registros 
        
 );  
 
    // Guardar datos de la etapa EX para utilizarlos en la etapa MEM
    reg    [NB_DATA  - 1 : 0]   ALU_Result;
    reg    [NB_DATA  - 1 : 0]   Mem_Data_Out;
    reg    [NB_DATA  - 1 : 0]   PC_Plus_Four;
    
    // Se?ales de control que deben pasarse de la etapa EX a la etapa WB
    reg    [NB_MUX4  - 1 : 0]   Mem_To_Reg;           // Selecciona el dato que pasa a escribirse en el registro
    reg                         Write_Reg;            // Se?al para escribir registros 
    
	// Se?ales para hazard detection 
    reg    [NB_RDIR  - 1 : 0]   rs;
    reg    [NB_RDIR  - 1 : 0]   rt;
    reg    [NB_RDIR  - 1 : 0]   rd;
    reg    [NB_RDIR  - 1 : 0]   Dest_Register;
    
    // Guardar datos de la etapa EX para utilizarlos en la etapa EX
    assign o_ALU_Result_WB           = ALU_Result;
    assign o_Mem_Data_Out_WB         = Mem_Data_Out;
    assign o_PC_Plus_Four_WB         = PC_Plus_Four;
    
    assign  o_rs_WB                  = rs;
    assign  o_rt_WB                  = rt;
    assign  o_rd_WB                  = rd;
    assign  o_Dest_Register_WB       = Dest_Register;
    
    // Se?ales de control que deben pasarse de la etapa EX a la etapa WB
    assign o_Mem_To_Reg_WB           = Mem_To_Reg;           // Selecciona el dato que pasa a escribirse en el registro
    assign o_Write_Reg_WB            = Write_Reg;            // Se?al para escribir registros
    
    always @(negedge i_clk)
    begin
        if(i_Reset)
        begin
            ALU_Result          <= 32'b0;
            Mem_Data_Out        <= 32'b0;
            PC_Plus_Four        <= 32'b0;
                  
            rs                  <= 5'b0;
            rt                  <= 5'b0;
            rd                  <= 5'b0;
            Dest_Register       <= 5'b0;              
                                
            Mem_To_Reg          <= 2'b0;    
            Write_Reg           <= 1'b0;       
        end
        else 
        begin
            ALU_Result          <= i_ALU_Result_MEM;
            Mem_Data_Out        <= i_Mem_Data_Out_MEM;
            PC_Plus_Four        <= i_PC_Plus_Four_MEM;
 
            rs                  <= i_rs_MEM;
            rt                  <= i_rt_MEM;
            rd                  <= i_rd_MEM;
            Dest_Register       <= i_Dest_Register_MEM;  
                                                     
            Mem_To_Reg          <= i_Mem_To_Reg_MEM;  
            Write_Reg           <= i_Write_Reg_MEM;          
        end    
    end
 
endmodule
