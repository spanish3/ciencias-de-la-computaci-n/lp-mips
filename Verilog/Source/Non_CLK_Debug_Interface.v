`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 12:37:48
// Design Name: 
// Module Name: Non_CLK_Debug_Interface
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Non_CLK_Debug_Interface 
#( 
    parameter   NB_DATA = 32,
    parameter   NB_UART = 8,    // Cantidad de bits de datos
    parameter   W = 8,          // # buffer de W bits
    parameter   OP_BITS = 6,    // Bits de operaciones
    parameter   NB_STAT = 4,    // Cantidad de bits para representar estados
    parameter   NB_SEND = 384, // Cantidad de bits a enviar (tabla)
    parameter   TC_WORD = 12,   // Cantidad de palabras de 32 bits en la tabla
    parameter   SB_TICK = 16, 
    parameter   NB_CONT = 4,
    parameter   NB_BRCV = 3,
    parameter   M = 14        // Param pra Baud Rate Generator
)
(
    input   wire                                    i_clk,                  // Clock
    input   wire                                    i_Reset,                // Reset
    input   wire            [NB_DATA  - 1 : 0]      i_CLP,              // Cantidad de lineas de programa
    input   wire            [NB_UART  - 1 : 0]      i_Mode_Op,
    input   wire                                    i_rx,    
    
    output  wire                                    o_tx,
    
    output  wire            [NB_STAT  - 1 : 0]      o_Debug_State,
    output  wire                                    o_Write_Hab,
    output  wire            [NB_SEND  - 1 : 0]      o_Table,
    output  wire            [NB_DATA  - 1 : 0]      o_Prog_Cont    
);

    localparam      [NB_STAT  - 1 : 0]   CLP1 = 4'b0000;      // Recibir dato LSB de CLP  (Cantidad de Lineas de Programa)
    localparam      [NB_STAT  - 1 : 0]   CLP2 = 4'b0001;      
    localparam      [NB_STAT  - 1 : 0]   CLP3 = 4'b0010;
    localparam      [NB_STAT  - 1 : 0]   CLP4 = 4'b0011;      // Recibir dato MSB de CLP  (Cantidad de Lineas de Programa)
    localparam      [NB_STAT  - 1 : 0]   MEMA = 4'b0100;      // Recibir dato LSB de una linea del programa
    localparam      [NB_STAT  - 1 : 0]   MEMB = 4'b0101;
    localparam      [NB_STAT  - 1 : 0]   MEMC = 4'b0110;
    localparam      [NB_STAT  - 1 : 0]   MEMD = 4'b0111;      // Recibir dato MSB de una linea del programa
    localparam      [NB_STAT  - 1 : 0]   EMOD = 4'b1000;      // Recibir dato Modo de Op (Continuo o paso)
    localparam      [NB_STAT  - 1 : 0]   EXEC = 4'b1001;      // Dar ok al MIPS para ejecutar durante 1 ciclo
    localparam      [NB_STAT  - 1 : 0]   REGA = 4'b1010;      // Enviar dato LSB de una linea de la tabla de registros (PC + 32 Reg + 10 Datamem[])
    localparam      [NB_STAT  - 1 : 0]   REGB = 4'b1011;
    localparam      [NB_STAT  - 1 : 0]   REGC = 4'b1100;
    localparam      [NB_STAT  - 1 : 0]   REGD = 4'b1101;      // Enviar dato LSB de una linea de la tabla de registros
    localparam      [NB_STAT  - 1 : 0]   EWRT = 4'b1111;      // Escribir archivo con la tabla recibida
    localparam      [NB_STAT  - 1 : 0]   EHLT = 4'b1110;      // Estado final: No hacer nada
     
    // uart
    wire                                rx_done_tick;
    wire                                tx_done_tick;
    wire            [NB_UART  - 1 : 0]  DEBUG_rx_data;
    
    // Interno
    reg             [NB_STAT  - 1 : 0]  state_reg, state_next;          // Registro de estado y estado siguiente
                            
    reg             [NB_DATA  - 1 : 0]  CONT, CONT_Next;                // Contador de Bytes enviados

    reg             [NB_UART  - 1 : 0]  Byte_MEM_In, Byte_MEM_In_Next;
    reg             [NB_UART  - 1 : 0]  Data_A_In, Data_A_In_Next;      // Registros para recibir bytes de la instruccion
    reg             [NB_UART  - 1 : 0]  Data_B_In, Data_B_In_Next;
    reg             [NB_UART  - 1 : 0]  Data_C_In, Data_C_In_Next;
    reg             [NB_UART  - 1 : 0]  Data_D_In, Data_D_In_Next;
    
    reg             [NB_UART  - 1 : 0]  Data_Out, Data_Out_Next;
    reg             [NB_UART  - 1 : 0]  Data_A_Out, Data_A_Out_Next;    // Registros para enviar bytes de la tabla
    reg             [NB_UART  - 1 : 0]  Data_B_Out, Data_B_Out_Next;
    reg             [NB_UART  - 1 : 0]  Data_C_Out, Data_C_Out_Next;
    reg             [NB_UART  - 1 : 0]  Data_D_Out, Data_D_Out_Next;
    
    reg             [NB_UART  - 1 : 0]  HLT, HLT_Next;
    
    reg             [NB_DATA  - 1 : 0]  table2      [0 : TC_WORD  - 1];
    reg            [NB_DATA  - 1 : 0]  Program     [0 : 1023];
              
    
    reg             [NB_DATA  - 1 : 0]  CONT2, CONT2_Next;              // Contador de Bytes enviados
    reg             [NB_DATA  - 1 : 0]  CONT3, CONT3_Next;              // Contador de Instrucciones Ejecutadas
    
    reg                                 tx_start_reg, tx_start_next;    // Registros para Iniciar la transmision
    
    genvar j;
    integer f;
    integer i;
    
    reg                                 write_file_hab, write_file_hab_next;
      
    initial
    begin
        #10
        f = $fopen("C:\\Users\\Lucas\\Desktop\\Arqui\\Code\\TPF-RC12\\Output.mem","wb");
        #10
        $readmemb("C:\\Users\\Lucas\\Desktop\\Arqui\\Code\\TPF-RC12\\Code.mem",Program);

    end
    
    assign  o_Prog_Mem_Byte = Byte_MEM_In;
    
    assign  o_Prog_Cont = CONT;
    assign  o_Write_Hab = write_file_hab;
    assign  o_Debug_State = state_reg;
     
    generate
        for(j = 0; j < TC_WORD; j = j + 1)
        begin
            assign  o_Table [NB_DATA - 1 + NB_DATA * j : 0 + NB_DATA * j] = table2 [j];
        end
    endgenerate 
    
    always @(posedge i_clk, posedge i_Reset)
    begin
        if(i_Reset)   // Ante un reset (o inicialmente) el estado actual es idle.
        begin
            state_reg           <= CLP1;
            Byte_MEM_In         <= 0;
            CONT                <= 0;
            Byte_MEM_In         <= 0;
            Data_Out            <= i_CLP [(NB_UART - 1 + NB_UART * 0) : (0 + NB_UART * 0)];
            Data_A_Out          <= 0;
            Data_B_Out          <= 0;
            Data_C_Out          <= 0;
            Data_D_Out          <= 0;
            HLT                 <= 0;
            CONT2               <= 0;
            CONT3               <= 0;
            tx_start_reg        <= 1;
            write_file_hab      <= 0;
        end
        else
        begin
            state_reg           <= state_next;
            CONT                <= CONT_Next;
            Byte_MEM_In         <= Byte_MEM_In_Next;
            Data_Out            <= Data_Out_Next;
            Data_A_Out          <= Data_A_Out_Next;
            Data_B_Out          <= Data_B_Out_Next;
            Data_C_Out          <= Data_C_Out_Next;
            Data_D_Out          <= Data_D_Out_Next;
            HLT                 <= HLT_Next;
            CONT2               <= CONT2_Next;
            CONT3               <= CONT3_Next;
            tx_start_reg        <= tx_start_next;
            write_file_hab      <= write_file_hab_next;   
        end
    end
    
    always @(*)
    begin
        state_next = state_reg;
        CONT_Next               = CONT;
        Byte_MEM_In_Next        = Byte_MEM_In;
        Data_Out_Next           = Data_Out;
        Data_A_Out_Next         = Data_A_Out;
        Data_B_Out_Next         = Data_B_Out;
        Data_C_Out_Next         = Data_C_Out;
        Data_D_Out_Next         = Data_D_Out;
        HLT_Next                = HLT;
        CONT2_Next              = CONT2;
        CONT3_Next              = CONT3;
        tx_start_next           = tx_start_reg;
        write_file_hab_next     = write_file_hab;     
        
        case(state_reg)
            CLP1:
            begin
                tx_start_next = 1'b1;
                Data_Out_Next = i_CLP [(NB_UART - 1 + NB_UART * 1) : (0 + NB_UART * 1)];
                if(tx_done_tick)
                begin
                    state_next = CLP2;
                end        
            end
            CLP2:
            begin
                Data_Out_Next = i_CLP [(NB_UART - 1 + NB_UART * 2) : (0 + NB_UART * 2)];
                if(tx_done_tick)
                begin
                    state_next = CLP3;
                end   
            end
            CLP3:
            begin
                Data_Out_Next = i_CLP [(NB_UART - 1 + NB_UART * 3) : (0 + NB_UART * 3)];
                if(tx_done_tick)
                begin
                    state_next = CLP4;
                end   
            end
            CLP4:
            begin
                if(tx_done_tick)
                begin
                    state_next = MEMA;
                    //Data_Out_Next = i_Prog_Data;
                    Data_Out_Next = Program[CONT][7 : 0];
                    tx_start_next = 1'b1;
                end   
            end
            MEMA:
            begin
                tx_start_next = 1'b1;            
                if(tx_done_tick)
                begin
                    state_next = MEMB;
                    //Data_Out_Next = i_Prog_Data;
                    Data_Out_Next = Program[CONT][15 : 8];
                end 
            end
            MEMB:
            begin
                if(tx_done_tick)
                begin
                    state_next = MEMC;
                    //Data_Out_Next = i_Prog_Data;
                    Data_Out_Next = Program[CONT][23 : 16];
                end 
            end
            MEMC:
            begin
                if(tx_done_tick)
                begin
                    state_next = MEMD;
                    //Data_Out_Next = i_Prog_Data;
                    Data_Out_Next = Program[CONT][31 : 24];
                end 
            end
            MEMD:
            begin        
                if(tx_done_tick)
                begin
                    if(CONT == i_CLP - 1)
                    begin
                        state_next = EMOD;
                        Data_Out_Next = i_Mode_Op;
                    end
                    else
                    begin
                        CONT_Next = CONT + 1;
                        state_next = MEMA;
                        //Data_Out_Next = i_Prog_Data;
                        Data_Out_Next = Program[CONT + 1][7 : 0];
                    end
                end 
            end
            EMOD:
            begin
                tx_start_next = 1'b1;
                if(tx_done_tick)
                begin                
                    tx_start_next = 1'b0;
                    CONT3_Next = CONT3 + 1;
                    state_next = REGA;
                end
            end
            REGA:
            begin
                if(rx_done_tick)
                begin
                    table2[CONT2][7 : 0] = DEBUG_rx_data;
                    state_next = REGB;
                end   
            end
            REGB:
            begin
                if(rx_done_tick)
                begin
                    
                    table2[CONT2][15 : 8] = DEBUG_rx_data;
                    state_next = REGC;
                end 
            end
            REGC:
            begin
                if(rx_done_tick)
                begin
                    table2[CONT2][23 : 16] = DEBUG_rx_data;
                    state_next = REGD;
                end 
            end
            REGD:
            begin
                if(rx_done_tick)
                begin
                    table2[CONT2][31 : 24] = DEBUG_rx_data;
                    if(CONT2 == TC_WORD - 1)    // Si se cumple esto se han recibido todos los elementos de la tabla
                    begin       
                        if(i_Mode_Op == 0)  // Si el modo era continuo y se encuentra en este estado, entonces ya se ha ejecutado el programa
                        begin
                            state_next = EWRT; 
                            tx_start_next = 1'b0;
                            f = $fopen("C:\\Users\\Lucas\\Desktop\\Arqui\\Code\\TPF-RC12\\Output.mem","wb");
                            write_file_hab_next = 0;                
                        end
                        else if(CONT3 != i_CLP) // Si no se debe volver a EMOD si el programa no se ha terminado o a EHLT si se ha terminado
                        begin
                            state_next = EMOD;
                            tx_start_next = 1'b1;
                            Data_Out_Next = i_Mode_Op;
                            CONT2_Next = 0;                        
                        end
                        else
                        begin
                            state_next = EWRT;
                            tx_start_next = 1'b0;
                            f = $fopen("C:\\Users\\Lucas\\Desktop\\Arqui\\Code\\TPF-RC12\\Output.mem","wb");
                            write_file_hab_next = 0; 
                        end
                    end
                    else
                    begin
                        CONT2_Next = CONT2_Next + 1;
                        state_next = REGA;
                        tx_start_next = 1'b0;
                    end
                end
            end
            EWRT:
            begin
                tx_start_next = 1'b0;
                if(i_Mode_Op == 0 || ((i_Mode_Op == 1) && (CONT3 == i_CLP)))
                begin
                    state_next = EHLT;
                    write_file_hab_next = 1;
                end
                else
                begin
                    state_next = EMOD;
                    tx_start_next = 1'b1;
                    Data_Out_Next = i_Mode_Op;
                    CONT2_Next = 0; 
                    write_file_hab_next = 0;
                end
                if(write_file_hab == 0)
                begin
                    $fdisplay(f,"PC : %d",table2[0]);
                    $fdisplay(f,"Cantidad de ciclos de clock : %d",table2[1]);
                    for(i = 0; i < 5; i = i + 1)
                    begin
                        //$fdisplay(f,"Reg %d = %d", i, table2[i + 2][7 : 0]);
                        $fdisplay(f,"Reg %d = %d", i, table2[i + 2]);
                    end
                    for(i = 0; i < 5; i = i + 1)
                    begin
                        //$fdisplay(f,"DataMem[%d] = %d", i, table2[i + 32][7 : 0]);
                        $fdisplay(f,"DataMem[%d] = %d", i, table2[i + 7]);
                    end
                    write_file_hab = 1;
                    write_file_hab_next = 1;
                    write_file_hab_next = 1;
                end            
            end
            EHLT:
            begin
                tx_start_next = 1'b0;
                state_next = EHLT;
                $fclose(f);
            end
            default:
            begin
                state_next = 4'b0;
            end
        endcase
    end

UART
#( 
    .NB_UART            (NB_UART),    // Cantidad de bits de datos
    .SB_TICK            (SB_TICK), 
    .NB_CONT            (NB_CONT),
    .NB_BRCV            (NB_BRCV),
    .M                  (M)        // Param pra Baud Rate Generator
 )
u_UART_1
(
    .i_clk              (i_clk), 
    .i_Reset            (i_Reset),
    .i_rx               (i_rx),
    .i_tx_data          (Data_Out),
    .i_tx_start         (tx_start_reg),
    
    .o_tx               (o_tx),
    .o_rx_done_tick     (rx_done_tick),
    .o_tx_done_tick     (tx_done_tick),
    .o_rx_data          (DEBUG_rx_data)
);

endmodule
