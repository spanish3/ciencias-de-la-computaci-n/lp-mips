`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:13:12
// Design Name: 
// Module Name: ID_EX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ID_EX
#(
    parameter   NB_DATA  = 32,
    parameter   NB_FUNC  = 6,
    parameter   NB_RDIR  = 5,
    parameter   NB_ALUOP = 4,
    parameter   NB_MUX4  = 2
 )
(
    // clk y reset
    input   wire                         i_clk,                  // Clock
    input   wire                         i_Reset,                // Reset
    
    // Guardar datos de la etapa ID para utilizarlos en la etapa EX
    input   wire    [NB_DATA  - 1 : 0]   i_Reg_Read_Data_1_ID,
    input   wire    [NB_DATA  - 1 : 0]   i_Reg_Read_Data_2_ID,
    input   wire    [NB_DATA  - 1 : 0]   i_Immediate_ID,
    input   wire    [NB_DATA  - 1 : 0]   i_PC_Plus_Four_ID,
    
    // Para saltos
    input   wire    [NB_DATA  - 1 : 0]   i_PC_Jump_ID,
    input   wire    [NB_DATA  - 1 : 0]   i_PC_Branch_Dir_ID,
    input   wire                         i_PC_Src_ID,
    input   wire                         i_Jump_ID,
    input   wire                         i_Jump_Register_ID,
    
    // Se?ales de control que deben pasarse de la etapa ID a la etapa EX
    input   wire    [NB_MUX4  - 1 : 0]   i_Reg_Dest_ID,
    input   wire                         i_ALU_Src_ID,
    input   wire    [NB_FUNC  - 1 : 0]   i_Function_ID,   
    input   wire    [NB_ALUOP - 1 : 0]   i_ALU_Operation_ID,
    
    // Se?ales de control que deben pasarse de la etapa ID a la etapa MEM
    input   wire                         i_Write_Reg_ID,            // Se?al para escribir registros 
    input   wire                         i_Branch_ID,               // Se?al para escribir registros 
    input   wire                         i_Mem_Read_ID,             // Leer memoria de datos
    input   wire                         i_Mem_Write_ID,            // Escribir memoria de datos
    input   wire    [NB_MUX4  - 1 : 0]   i_BHW_ID,                  // Lectura/escritura de Word, Half Word o Byte
    input   wire                         i_Signed_ID,                  //  
    
    // Se?ales de control que deben pasarse de la etapa ID a la etapa WB
    input   wire    [NB_MUX4  - 1 : 0]   i_Mem_To_Reg_ID,           // Selecciona el dato que pasa a escribirse en el registro
    
	// Se?ales para hazard detection 
    input   wire    [NB_RDIR  - 1 : 0]   i_rs_ID,
    input   wire    [NB_RDIR  - 1 : 0]   i_rt_ID,
    input   wire    [NB_RDIR  - 1 : 0]   i_rd_ID,
    
    input   wire                         i_Stall,                // Detener pipeline ante por ej riesgos de control	 

    //Salidas
        // Guardar datos de la etapa ID para utilizarlos en la etapa EX
    output  wire    [NB_DATA  - 1 : 0]   o_Reg_Read_Data_1_EX,
    output  wire    [NB_DATA  - 1 : 0]   o_Reg_Read_Data_2_EX,
    output  wire    [NB_DATA  - 1 : 0]   o_Immediate_EX,
    output  wire    [NB_DATA  - 1 : 0]   o_PC_Plus_Four_EX,
    
    // Para saltos
    output  wire    [NB_DATA  - 1 : 0]   o_PC_Jump_EX,
    output  wire    [NB_DATA  - 1 : 0]   o_PC_Branch_Dir_EX,
    output  wire                         o_PC_Src_EX,
    output  wire                         o_Jump_EX,
    output  wire                         o_Jump_Register_EX,
    
    // Se?ales de control que deben pasarse de la etapa ID a la etapa EX
    output  wire    [NB_MUX4  - 1 : 0]   o_Reg_Dest_EX,
    output  wire                         o_ALU_Src_EX,
    output  wire    [NB_FUNC  - 1 : 0]   o_Function_EX,
    output  wire    [NB_ALUOP - 1 : 0]   o_ALU_Operation_EX,  
    
    // Se?ales de control que deben pasarse de la etapa ID a la etapa MEM
    output  wire                         o_Write_Reg_EX,            // Se?al para escribir registros 
    output  wire                         o_Branch_EX,               // Se?al para escribir registros 
    output  wire                         o_Mem_Read_EX,             // Leer memoria de datos
    output  wire                         o_Mem_Write_EX,            // Escribir memoria de datos
    output  wire    [NB_MUX4  - 1 : 0]   o_BHW_EX,                     // Lectura/escritura de Word, Half Word o Byte
    output  wire                         o_Signed_EX,                  // Con o sin signo
    
    // Se?ales de control que deben pasarse de la etapa ID a la etapa WB
    output  wire    [NB_MUX4  - 1 : 0]   o_Mem_To_Reg_EX,           // Selecciona el dato que pasa a escribirse en el registro
    
	// Se?ales para hazard detection 
    output  wire    [NB_RDIR  - 1 : 0]   o_rs_EX,
    output  wire    [NB_RDIR  - 1 : 0]   o_rt_EX,
    output  wire    [NB_RDIR  - 1 : 0]   o_rd_EX
    
 );  
 
    // Guardar datos de la etapa ID para utilizarlos en la etapa EX
    reg    [NB_DATA  - 1 : 0]   Reg_Read_Data_1;
    reg    [NB_DATA  - 1 : 0]   Reg_Read_Data_2;
    reg    [NB_DATA  - 1 : 0]   Immediate;
    reg    [NB_DATA  - 1 : 0]   PC_Plus_Four;
    
    // Para saltos
    reg    [NB_DATA  - 1 : 0]   PC_Jump;
    reg    [NB_DATA  - 1 : 0]   PC_Branch_Dir;
    reg                         PC_Src;
    reg                         Jump;
    reg                         Jump_Register;
    
    // Se?ales de control que deben pasarse de la etapa ID a la etapa EX
    reg    [NB_MUX4  - 1 : 0]   Reg_Dest;
    reg                         ALU_Src;
    reg    [NB_FUNC  - 1 : 0]   Function;
    reg    [NB_ALUOP - 1 : 0]   ALU_Operation;  
    
    // Se?ales de control que deben pasarse de la etapa ID a la etapa MEM
    reg                         Write_Reg;            // Se?al para escribir registros 
    reg                         Branch;               // Se?al para escribir registros 
    reg                         Mem_Read;             // Leer memoria de datos
    reg                         Mem_Write;            // Escribir memoria de datos
    reg    [NB_MUX4  - 1 : 0]   BHW;                     // Lectura/escritura de Word; Half Word o Byte
    reg                         Signed;
    
    // Se?ales de control que deben pasarse de la etapa ID a la etapa WB
    reg    [NB_MUX4  - 1 : 0]   Mem_To_Reg;           // Selecciona el dato que pasa a escribirse en el registro
    
	// Se?ales para hazard detection 
    reg    [NB_RDIR  - 1 : 0]   rs;
    reg    [NB_RDIR  - 1 : 0]   rt;
    reg    [NB_RDIR  - 1 : 0]   rd;
    
    // Guardar datos de la etapa ID para utilizarlos en la etapa EX
    assign o_Reg_Read_Data_1_EX = Reg_Read_Data_1;
    assign o_Reg_Read_Data_2_EX = Reg_Read_Data_2;
    assign o_Immediate_EX       = Immediate;
    assign o_PC_Plus_Four_EX   = PC_Plus_Four;
    
    // Para saltos
    assign o_PC_Jump_EX = PC_Jump;
    assign o_PC_Branch_Dir_EX = PC_Branch_Dir;
    assign o_PC_Src_EX = PC_Src;
    assign o_Jump_EX = Jump;
    assign o_Jump_Register_EX = Jump_Register;
    
    // Se?ales de control que deben pasarse de la etapa ID a la etapa EX
    assign o_Reg_Dest_EX        = Reg_Dest;
    //assign o_ALU_Dec_Op_EX      = ALU_Dec_Op;
    assign o_ALU_Src_EX         = ALU_Src;
    assign o_Function_EX        = Function;
    assign o_ALU_Operation_EX   = ALU_Operation;
    
    // Se?ales de control que deben pasarse de la etapa ID a la etapa MEM
    assign o_Write_Reg_EX       = Write_Reg;            // Se?al para escribir registros 
    assign o_Branch_EX          = Branch;               // Se?al para escribir registros 
    assign o_Mem_Read_EX        = Mem_Read;             // Leer memoria de datos
    assign o_Mem_Write_EX       = Mem_Write;            // Escribir memoria de datos
    assign o_BHW_EX             = BHW;                     // Lectura/escritura de Word; Half Word o Byte
    assign o_Signed_EX          = Signed;
    
    // Se?ales de control que deben pasarse de la etapa ID a la etapa WB
    assign o_Mem_To_Reg_EX      = Mem_To_Reg;           // Selecciona el dato que pasa a escribirse en el registro
    
	// Se?ales para hazard detection 
    assign  o_rs_EX             = rs;
    assign  o_rt_EX             = rt;
    assign  o_rd_EX             = rd;

    //always @(negedge i_clk)
    always @(posedge i_clk)
    begin
        if(i_Reset)
        begin
            Reg_Read_Data_1 <= 32'b0;
            Reg_Read_Data_2 <= 32'b0;
            Immediate       <= 32'b0;
            PC_Plus_Four    <= 32'b0;
            
            PC_Jump         <= 32'b0;
            PC_Branch_Dir   <= 32'b0;
            PC_Src          <= 1'b0;
            Jump            <= 1'b0;
            Jump_Register   <= 1'b0;
    
            Reg_Dest        <= 2'b0;
            ALU_Src         <= 1'b0;
            Function        <= 6'b0;
            ALU_Operation   <= 4'b0;
    
            Write_Reg       <= 1'b0;            
            Branch          <= 1'b0;               
            Mem_Read        <= 1'b0;           
            Mem_Write       <= 1'b0;            
            BHW             <= 2'b0;
            Signed          <= 1'b0;                    
    
            Mem_To_Reg      <= 2'b0;           
    
            rs              <= 5'b0;
            rt              <= 5'b0;
            rd              <= 5'b0;
        end
        else if (i_Stall)
        begin
            Reg_Read_Data_1 <= 32'b0;
            Reg_Read_Data_2 <= 32'b0;
            Immediate       <= 32'b0;
            PC_Plus_Four    <= i_PC_Plus_Four_ID;

            PC_Jump         <= 32'b0;
            PC_Branch_Dir   <= 32'b0;
            PC_Src          <= 1'b0;
            Jump            <= 1'b0;
            Jump_Register   <= 1'b0;
    
            Reg_Dest        <= 2'b0;
            ALU_Src         <= 1'b0;
            Function        <= 6'b0;
            ALU_Operation   <= 4'b0;
    
            Write_Reg       <= 1'b0;            
            Branch          <= 1'b0;               
            Mem_Read        <= 1'b0;           
            Mem_Write       <= 1'b0;            
            BHW             <= 2'b0;
            Signed          <= 1'b0;                      
    
            Mem_To_Reg      <= 2'b0;           
    
            rs              <= 5'b0;
            rt              <= 5'b0;
            rd              <= 5'b0;        
        end
        else 
        begin
            Reg_Read_Data_1 <= i_Reg_Read_Data_1_ID;
            Reg_Read_Data_2 <= i_Reg_Read_Data_2_ID;
            Immediate       <= i_Immediate_ID;
            PC_Plus_Four    <= i_PC_Plus_Four_ID;
            
            PC_Jump         <= i_PC_Jump_ID;
            PC_Branch_Dir   <= i_PC_Branch_Dir_ID;
            PC_Src          <= i_PC_Src_ID;
            Jump            <= i_Jump_ID;
            Jump_Register   <= i_Jump_Register_ID;
    
            Reg_Dest        <= i_Reg_Dest_ID;
            ALU_Src         <= i_ALU_Src_ID;
            Function        <= i_Function_ID;
            ALU_Operation   <= i_ALU_Operation_ID;
    
            Write_Reg       <= i_Write_Reg_ID;            
            Branch          <= i_Branch_ID;               
            Mem_Read        <= i_Mem_Read_ID;           
            Mem_Write       <= i_Mem_Write_ID;            
            BHW             <= i_BHW_ID;
            Signed          <= i_Signed_ID;                      
    
            Mem_To_Reg      <= i_Mem_To_Reg_ID;           
    
            rs              <= i_rs_ID;
            rt              <= i_rt_ID;
            rd              <= i_rd_ID;        
        end    
    end
    
 
endmodule
