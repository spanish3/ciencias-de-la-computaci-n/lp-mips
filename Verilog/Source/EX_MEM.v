`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 10:50:18
// Design Name: 
// Module Name: EX_MEM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module EX_MEM
#(
    parameter   NB_DATA  = 32,
    parameter   NB_RDIR  = 5,
    parameter   NB_ALUOP = 4,
    parameter   NB_MUX4  = 2
 )
(
    // clk y reset
    input   wire                         i_clk,                  // Clock
    input   wire                         i_Reset,                // Reset
    
    // Guardar datos de la etapa EX para utilizarlos en la etapa MEM
    input   wire    [NB_DATA  - 1 : 0]   i_ALU_Result_EX,       // Resultado de la ALU
    input   wire    [NB_DATA  - 1 : 0]   i_Reg_Read_Data_2_EX,  // Para escribir en memoria desde el banco de registros
    input   wire    [NB_DATA  - 1 : 0]   i_PC_Plus_Four_EX,    // PC + 4
    
    input   wire    [NB_RDIR  - 1 : 0]   i_rs_EX,
    input   wire    [NB_RDIR  - 1 : 0]   i_rt_EX,
    input   wire    [NB_RDIR  - 1 : 0]   i_rd_EX,
    input   wire    [NB_RDIR  - 1 : 0]   i_Dest_Register_EX,    // Registro de escritura
    
    // Se?ales de control que deben pasarse de la etapa EX a la etapa MEM
    input   wire                         i_Mem_Read_EX,             // Leer memoria de datos
    input   wire                         i_Mem_Write_EX,            // Escribir memoria de datos
    input   wire                         i_Signed_EX,            // Escribir memoria de datos
    input   wire    [NB_MUX4  - 1 : 0]   i_BHW_EX,                     // Lectura/escritura de Word, Half Word o Byte
    
    // Se?ales de control que deben pasarse de la etapa EX a la etapa WB
    input   wire    [NB_MUX4  - 1 : 0]   i_Mem_To_Reg_EX,           // Selecciona el dato que pasa a escribirse en el registro
    input   wire                         i_Write_Reg_EX,            // Se?al para escribir registros 
    
    //Salidas
        // Guardar datos de la etapa EX para utilizarlos en la etapa EX
    output  wire    [NB_DATA  - 1 : 0]   o_ALU_Result_MEM,       // Resultado de la ALU
    output  wire    [NB_DATA  - 1 : 0]   o_Reg_Read_Data_2_MEM,
    output  wire    [NB_DATA  - 1 : 0]   o_PC_Plus_Four_MEM,
    
    output  wire    [NB_RDIR  - 1 : 0]   o_rs_MEM,
    output  wire    [NB_RDIR  - 1 : 0]   o_rt_MEM,
    output  wire    [NB_RDIR  - 1 : 0]   o_rd_MEM,
    output  wire    [NB_RDIR  - 1 : 0]   o_Dest_Register_MEM,    // Registro de escritura
    
    // Se?ales de control que deben pasarse de la etapa EX a la etapa MEM
    output  wire                         o_Mem_Read_MEM,             // Leer memoria de datos
    output  wire                         o_Mem_Write_MEM,            // Escribir memoria de datos
    output  wire    [NB_MUX4  - 1 : 0]   o_BHW_MEM,                  // Lectura/escritura de Word, Half Word o Byte
    output  wire                         o_Signed_MEM,               // Se?al para escribir registros 
    
    // Se?ales de control que deben pasarse de la etapa EX a la etapa WB
    output  wire    [NB_MUX4  - 1 : 0]   o_Mem_To_Reg_MEM,           // Selecciona el dato que pasa a escribirse en el registro
    output  wire                         o_Write_Reg_MEM            // Se?al para escribir registros 
        
 );  
 
    // Guardar datos de la etapa EX para utilizarlos en la etapa MEM
    reg    [NB_DATA  - 1 : 0]   ALU_Result;
    reg    [NB_DATA  - 1 : 0]   Reg_Read_Data_2;
    reg    [NB_DATA  - 1 : 0]   PC_Plus_Four;
    
    
    // Se?ales de control que deben pasarse de la etapa EX a la etapa MEM
    reg                         Mem_Read;             // Leer memoria de datos
    reg                         Mem_Write;            // Escribir memoria de datos
    reg    [NB_MUX4  - 1 : 0]   BHW;                     // Lectura/escritura de Word; Half Word o Byte
    reg                         Signed;             // Leer memoria de datos
    
    // Se?ales de control que deben pasarse de la etapa EX a la etapa WB
    reg    [NB_MUX4  - 1 : 0]   Mem_To_Reg;           // Selecciona el dato que pasa a escribirse en el registro
    reg                         Write_Reg;            // Se?al para escribir registros 
    
	// Se?ales para hazard detection 
    reg    [NB_RDIR  - 1 : 0]   rs;
    reg    [NB_RDIR  - 1 : 0]   rt;
    reg    [NB_RDIR  - 1 : 0]   rd;
    reg    [NB_RDIR  - 1 : 0]   Dest_Register;
    
    // Guardar datos de la etapa EX para utilizarlos en la etapa EX
    assign o_ALU_Result_MEM         = ALU_Result;
    assign o_Reg_Read_Data_2_MEM    = Reg_Read_Data_2;
    assign o_PC_Plus_Four_MEM       = PC_Plus_Four;
    
    assign  o_rs_MEM                = rs;
    assign  o_rt_MEM                = rt;
    assign  o_rd_MEM                = rd;
    assign  o_Dest_Register_MEM     = Dest_Register;
    
    // Se?ales de control que deben pasarse de la etapa EX a la etapa MEM 
    assign o_Mem_Read_MEM           = Mem_Read;             // Leer memoria de datos
    assign o_Mem_Write_MEM          = Mem_Write;            // Escribir memoria de datos
    assign o_BHW_MEM                = BHW;                     // Lectura/escritura de Word; Half Word o Byte
    assign o_Signed_MEM             = Signed;  
    
    
    // Se?ales de control que deben pasarse de la etapa EX a la etapa WB
    assign o_Mem_To_Reg_MEM         = Mem_To_Reg;           // Selecciona el dato que pasa a escribirse en el registro
    assign o_Write_Reg_MEM          = Write_Reg;            // Se?al para escribir registros
    

    always @(negedge i_clk)
    begin
        if(i_Reset)
        begin
            ALU_Result          <= 32'b0;
            Reg_Read_Data_2     <= 32'b0;
            PC_Plus_Four        <= 32'b0;
                  
            rs                  <= 5'b0;
            rt                  <= 5'b0;
            rd                  <= 5'b0;
            Dest_Register       <= 5'b0;              
                  
            Mem_Read            <= 1'b0;           
            Mem_Write           <= 1'b0;            
            BHW                 <= 2'b0;
            Signed              <= 1'b0;                     
    
            Mem_To_Reg          <= 2'b0;    
            Write_Reg           <= 1'b0;       
        end
        else 
        begin
            ALU_Result          <= i_ALU_Result_EX;
            Reg_Read_Data_2     <= i_Reg_Read_Data_2_EX;
            PC_Plus_Four        <= i_PC_Plus_Four_EX;
            
            rs                  <= i_rs_EX;
            rt                  <= i_rt_EX;
            rd                  <= i_rd_EX;
            Dest_Register       <= i_Dest_Register_EX;  
                                    
            Mem_Read            <= i_Mem_Read_EX;           
            Mem_Write           <= i_Mem_Write_EX;            
            BHW                 <= i_BHW_EX;    
            Signed              <= i_Signed_EX;                   
    
            Mem_To_Reg          <= i_Mem_To_Reg_EX;  
            Write_Reg           <= i_Write_Reg_EX;          
        end    
    end
    
endmodule
