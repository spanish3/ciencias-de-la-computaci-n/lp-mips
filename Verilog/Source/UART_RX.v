`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:15:07
// Design Name: 
// Module Name: UART_RX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module UART_RX
#(
    parameter   NB_UART = 8,    // Cantidad de bits de datos
    parameter   SB_TICK = 16,   // Cantidad de ticks necesarios para los bits de stop
    parameter   NB_STAT = 2,    // Cantidad de bits para representar los estdos    
    parameter   NB_CONT = 4,
    parameter   NB_BRCV = 3
 )
 (
    // Entradas
    input   wire                                i_clk,
    input   wire                                i_Reset,        
    input   wire                                i_rx,           // rx
    input   wire                                s_tick,         // Se?al de Baud Rate Gen

    // Salidas
    output  reg                                 rx_done_tick,   // Indica que el dato ha sido recibido
    output  wire    signed  [NB_UART - 1 : 0]   dout
 );
 
    // EStados
    localparam              [NB_STAT - 1 : 0]   idle  = 2'b00;
    localparam              [NB_STAT - 1 : 0]   start = 2'b01;   // s_reg cuenta hasta 7
    localparam              [NB_STAT - 1 : 0]   data  = 2'b10;   // s_reg cuenta hasta 15
    localparam              [NB_STAT - 1 : 0]   stop  = 2'b11;   // s_reg cuenta hasta SB_TICK
    
    // Interno
    reg                     [NB_STAT - 1 : 0]   state_reg;      // Estado actual y estado siguiente
    reg                     [NB_STAT - 1 : 0]   state_next;     // s_reg cuenta la cantidad de ticks que ocurrieron
    reg                     [NB_CONT - 1 : 0]   s_reg;          
    reg                     [NB_CONT - 1 : 0]   s_next;
    reg                     [NB_BRCV - 1 : 0]   n_reg;          // n_reg cuenta la cantidad de bits recibidos
    reg                     [NB_BRCV - 1 : 0]   n_next;
    reg     signed          [NB_UART - 1 : 0]   b_reg;          // b_reg guarda los datos recibidos (paralelizar)
    reg     signed          [NB_UART - 1 : 0]   b_next;
    
    // Estados - Secuencial
    always@(posedge i_clk, posedge i_Reset)
    begin
        if(i_Reset)
        begin
            state_reg <= idle;
            s_reg <= 0;
            n_reg <= 0;
            b_reg <= 0;
        end
        else
        begin
            state_reg <= state_next;
            s_reg <= s_next;
            n_reg <= n_next;
            b_reg <= b_next;
        end
    end
    
    // Obtener el siguiente estado - Combinacional
    always@(*)
    begin
        // Valores por defecto: Estado siguiente = estado actual, rx_done_tick = 0
        state_next = state_reg;
        rx_done_tick = 1'b0;
        s_next = s_reg;
        n_next = n_reg;
        b_next = b_reg;
        
        case(state_reg)
            idle:
                if (~i_rx)              // Si rx == 0 entonces
                begin
                    state_next = start; // se debe comenzar a transmitir
                    s_next = 0;         // La cuenta de ticks sigue en cero
                end
            start:
                if(s_tick)
                begin
                    if(s_reg == 7)      // Al medio del periodo del bit de start
                    begin 
                        state_next = data;
                        s_next = 0;
                        n_next = 0;
                    end
                    else
                    begin
                        s_next = s_reg + 1; // Incrementar el conador de ticks salvo cuando llega a 7 (mitad de la cant de ticks)
                    end 
                end                              
            data:
            if(s_tick)  // Ante un tick
                if(s_reg == 15)
                    begin
                        s_next = 0; // Resetar cuenta de ticks
                        b_next = {i_rx, b_reg [7 : 1]}; // Concatenar rx con b_reg donde se arma el dato con los bits de rx
                        if(n_reg == (NB_UART - 1)) // Si ya se tienen todos los datos el siguiente estado ser? STOP
                            state_next = stop;  // e? contador n_reg se resetea en el estado START.
                        else
                            n_next = n_reg + 1; // Incrementar el contador de datos secibidos a no ser que llegue a DBIT -1
                    end
                else
                    s_next = s_reg + 1;     // Incrementar la cuenta de ticks a no ser que se llegue a 15
        stop:
            if(s_tick)  // Ante un tick
                if(s_reg == (SB_TICK - 1))
                    begin
                        state_next = idle;      // Volver al estado inicial esperando otra transferencia.    
                        rx_done_tick = 1'b1;    // Si ya estan todos los ticks en estado stop, el proceso termin?
                    end      
                else
                    s_next = s_reg + 1; // Incrementar el contador de ticks hasta que se llegue al valor max                                                                       
    endcase     
end

// Salida
assign dout = b_reg;

endmodule
