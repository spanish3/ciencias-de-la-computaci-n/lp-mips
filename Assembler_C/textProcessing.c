#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>  // isdigit()

#define TAM 1024

void separarComentarios(char **args, char *line, char *aux, int position, int buffsize)
{
  //strtok(char *str, const char *delim) separa str cada vez que 
  //encuentra algo contenido en delim  
  aux = strtok(line,";");
  while(aux != NULL)
  {
    args[position] = aux;
    position++;
    if(position >= buffsize)	// Si la cantidad de elementos excede 1000
    {
      buffsize += TAM;	// Se agregan 1000 más a buffsize
      args = realloc(aux, buffsize * sizeof(char*));	// Se agranda el bloque de memoria de Argumentos coon el nuevo tamaño
      if (!args) 
      {
        fprintf(stderr, "Error de asignación de memoria\n");	// Ante un fallo: mensaje y salir
        exit(EXIT_FAILURE);
      }
    }
    //aux = strtok(NULL," \n");	// siguiente palabra
    aux = strtok(NULL,";");	// siguiente palabra
  }
  args[position] = NULL;
}

void separarPalabras(char **args, char *line, char *aux, int position, int buffsize)
{
  position = 0;
  aux = strtok(args[0]," ,\n");
  while(aux != NULL)
  {
    args[position] = aux;
    position++;
    if(position >= buffsize)	// Si la cantidad de elementos excede 1000
    {
      buffsize += TAM;	// Se agregan 1000 más a buffsize
      args = realloc(aux, buffsize * sizeof(char*));	// Se agranda el bloque de memoria de Argumentos coon el nuevo tamaño
      if (!args) 
      {
        fprintf(stderr, "Error de asignación de memoria\n");	// Ante un fallo: mensaje y salir
        exit(EXIT_FAILURE);
      }
    }
    aux = strtok(NULL," ,\n()$");	// siguiente palabra
  }
  args[position] = NULL;
  /*int i = 0;
  while(args[i] != NULL)
  {
    printf("Argumento %d: %s\n",i,args[i]);
    i++;
  }*/
}

int isMnemonic(char *w)
{
  char *Instrucciones[] = {"addu","subu","and","or","xor","nor","slr","sllv","srlv","srav","sll","srl","sra","lb","lh","lw","lwu","lbu","lhu",
    "sb","sh","sw","lui","addi","andi","ori","xori","slti","beq","bne","j","jal","jr","jalr","nop","hlt"};

  //printf("Se ingresa %s\n",w);
   
   for(int i = 0; i < 36; i ++)
   {
     if(!strcmp(w,Instrucciones[i]))  // strcmp retorna 0 si los strings son iguales
     {
       return 1;
     }
   }
   return 0;
}

int isNumeric(char *string)
{
  for(int i = 0; i < strlen(string); i++)
  {
    if(!isdigit(string[i]))
    {
      return 0;
    }
  }
  return 1;
}
