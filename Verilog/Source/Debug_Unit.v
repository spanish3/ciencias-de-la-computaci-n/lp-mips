`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:16:30
// Design Name: 
// Module Name: Debug_Unit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Debug_Unit
#( 
    parameter   NB_DATA = 32,
    parameter   NB_UART = 8,    // Cantidad de bits de datos
    parameter   W = 8,          // # buffer de W bits
    parameter   OP_BITS = 6,    // Bits de operaciones
    parameter   NB_STAT = 4,    // Cantidad de bits para representar estados
    parameter   NB_SEND = 384, // Cantidad de bits a enviar (tabla)
    parameter   TC_WORD = 12,   // Cantidad de palabras de 32 bits en la tabla
    parameter   SB_TICK = 16, 
    parameter   NB_CONT = 4,
    parameter   NB_BRCV = 3,
    parameter   M = 14        // Param para Baud Rate Generator
)
(
    input   wire                                    i_clk,                  // Clock
    input   wire                                    i_Reset,                // Reset,         
    
    // Entradas Rx
    input   wire                                    i_rx,

    // Entradas MIPS
    input   wire                                    i_hlt,
    input   wire              [NB_SEND  - 1 : 0]    i_table,   
    
    // Salidas Program Memory
    output  wire              [NB_DATA  - 1 : 0]    o_Prog_Mem_Address,
    output  wire              [NB_UART  - 1 : 0]    o_Prog_Mem_Byte,
    output  wire                                    o_Prog_Mem_Write,
    
    // Salidas MIPS
    output  wire                                    o_Debug_Stall,
    
    // Salidas Tx
    output  wire                                    o_tx   
    
);

    localparam      [NB_STAT  - 1 : 0]   CLP1 = 4'b0000;      // Recibir dato LSB de CLP  (Cantidad de Lineas de Programa)
    localparam      [NB_STAT  - 1 : 0]   CLP2 = 4'b0001;      
    localparam      [NB_STAT  - 1 : 0]   CLP3 = 4'b0010;
    localparam      [NB_STAT  - 1 : 0]   CLP4 = 4'b0011;      // Recibir dato MSB de CLP  (Cantidad de Lineas de Programa)
    localparam      [NB_STAT  - 1 : 0]   MEMA = 4'b0100;      // Recibir dato LSB de una linea del programa
    localparam      [NB_STAT  - 1 : 0]   MEMB = 4'b0101;
    localparam      [NB_STAT  - 1 : 0]   MEMC = 4'b0110;
    localparam      [NB_STAT  - 1 : 0]   MEMD = 4'b0111;      // Recibir dato MSB de una linea del programa
    localparam      [NB_STAT  - 1 : 0]   EMOD = 4'b1000;      // Recibir dato Modo de Op (Continuo o paso)
    localparam      [NB_STAT  - 1 : 0]   EXEC = 4'b1001;      // Dar ok al MIPS para ejecutar durante 1 ciclo
    localparam      [NB_STAT  - 1 : 0]   REGA = 4'b1010;      // Enviar dato LSB de una linea de la tabla de registros (PC + 32 Reg + 10 Datamem[])
    localparam      [NB_STAT  - 1 : 0]   REGB = 4'b1011;
    localparam      [NB_STAT  - 1 : 0]   REGC = 4'b1100;
    localparam      [NB_STAT  - 1 : 0]   REGD = 4'b1101;      // Enviar dato LSB de una linea de la tabla de registros
    localparam      [NB_STAT  - 1 : 0]   EHLT = 4'b1110;      // Estado final: No hacer nada
    
    // UART TX y RX
    wire            [NB_UART  - 1 : 0]  MIPS_rx_data;
    wire                                rx_done_tick;
    wire                                tx_done_tick;
    reg             [NB_UART  - 1 : 0]  Data_Out, Data_Out_Next;        // Registro para enviar bytes de la tabla   
    reg                                 tx_start_reg, tx_start_next;    // Registros para Iniciar la transmision
    
    // Estado
    reg             [NB_STAT  - 1 : 0]  state_reg, state_next;          // Registro de estado y estado siguiente
    
    // CLP = Cantidad de Lineas de Programa (se divide en 4 Bytes)
    wire            [NB_DATA  - 1 : 0]  CLP;                            // Cantidad de lineas de program
    reg             [NB_UART  - 1 : 0]  CLP_A_In, CLP_A_In_Next;        // Registros para recibir bytes de CLP
    reg             [NB_UART  - 1 : 0]  CLP_B_In, CLP_B_In_Next;
    reg             [NB_UART  - 1 : 0]  CLP_C_In, CLP_C_In_Next;
    reg             [NB_UART  - 1 : 0]  CLP_D_In, CLP_D_In_Next;
    
    assign  CLP = {CLP_D_In,CLP_C_In,CLP_B_In,CLP_A_In};                // CLP esta formado por la concat de los 4 bytes CLPi recibidos
    
    // Contadores
    reg             [NB_DATA  - 1 : 0]  CONT, CONT_Next;                // Contador de Bytes escritos en memoria
    reg             [NB_DATA  - 1 : 0]  CONT2, CONT2_Next;              // Contador de Bytes enviados
    
    // Para escribir en memoria de programa
    reg             [NB_UART  - 1 : 0]  Byte_MEM_In, Byte_MEM_In_Next;  // Registro para recibir bytes de la instruccion 
    reg             [NB_DATA  - 1 : 0]  Prog_Mem_Address, Prog_Mem_Address_Next;
    reg                                 Prog_Mem_Write, Prog_Mem_Write_Next;
    
    // Operation mode (continuo "0" o paso "!=0")
    reg             [NB_UART  - 1 : 0]  Mode_Op, Mode_Op_Next;
    
    // Tabla con los 44 registros que se deben enviar (32 registros del banco de registros, PC, cantidad de clocks desde el inicio, 10 posiciones de data mem)
    wire             [NB_DATA  - 1 : 0]  table2      [0 : TC_WORD  - 1];
    
    // Detener el procesador
    reg                                 Debug_Stall, Debug_Stall_Next;
    
    genvar i;
    
    assign  o_Prog_Mem_Byte = Byte_MEM_In;
    assign  o_Prog_Mem_Address = Prog_Mem_Address;
    assign  o_Prog_Mem_Write = Prog_Mem_Write;
    assign  o_tx_start = tx_start_reg;
    assign  o_Debug_Stall = Debug_Stall;
    assign  dout = Data_Out;
    
    generate
        for(i = 0; i < TC_WORD; i = i + 1)
        begin
            assign table2 [i] = i_table[(NB_DATA  - 1 + NB_DATA * i) : (0 + NB_DATA * i)];
        end
    endgenerate
    
    always @(posedge i_clk, posedge i_Reset)
    begin
        if(i_Reset)   // Ante un reset (o inicialmente) el estado actual es idle.
        begin
            state_reg           <= CLP1;
            Byte_MEM_In         <= 0;
            CLP_A_In            <= 0;
            CLP_B_In            <= 0;
            CLP_C_In            <= 0;
            CLP_D_In            <= 0;
            CONT                <= 0;
            Byte_MEM_In         <= 0;
            Prog_Mem_Address    <= 0;
            Prog_Mem_Write      <= 0;
            Debug_Stall         <= 1;
            Mode_Op             <= 0;
            Data_Out            <= 0;
            CONT2               <= 0;
            tx_start_reg        <= 0;
        end
        else
        begin
            state_reg           <= state_next;
            CLP_A_In            <= CLP_A_In_Next;
            CLP_B_In            <= CLP_B_In_Next;
            CLP_C_In            <= CLP_C_In_Next;
            CLP_D_In            <= CLP_D_In_Next;
            CONT                <= CONT_Next;
            Byte_MEM_In         <= Byte_MEM_In_Next;
            Prog_Mem_Address    <= Prog_Mem_Address_Next;
            Prog_Mem_Write      <= Prog_Mem_Write_Next;
            Debug_Stall         <= Debug_Stall_Next;
            Mode_Op             <= Mode_Op_Next;
            Data_Out            <= Data_Out_Next;
            CONT2               <= CONT2_Next;
            tx_start_reg        <= tx_start_next;   
        end
    end
    
    always @(*)
    begin
        state_next = state_reg;
        CLP_A_In_Next           = CLP_A_In;
        CLP_B_In_Next           = CLP_B_In;
        CLP_C_In_Next           = CLP_C_In;
        CLP_D_In_Next           = CLP_D_In;
        CONT_Next               = CONT;
        Byte_MEM_In_Next        = Byte_MEM_In;
        Prog_Mem_Address_Next   = Prog_Mem_Address;
        Prog_Mem_Write_Next     = Prog_Mem_Write;
        Debug_Stall_Next        = Debug_Stall;
        Mode_Op_Next            = Mode_Op;
        Data_Out_Next           = Data_Out;
        CONT2_Next              = CONT2;
        tx_start_next           = tx_start_reg;  
        
        case(state_reg)
            CLP1:
            begin
                tx_start_next = 1'b0;
                if(rx_done_tick)
                begin
                    CLP_A_In_Next = MIPS_rx_data;
                    state_next = CLP2;
                end
            end
            CLP2:
            begin
                if(rx_done_tick)
                begin
                    CLP_B_In_Next = MIPS_rx_data;
                    state_next = CLP3;
                end
            end 
            CLP3:
            begin
                if(rx_done_tick)
                begin
                    CLP_C_In_Next = MIPS_rx_data;
                    state_next = CLP4;
                end
            end
            CLP4:
            begin
                if(rx_done_tick)
                begin
                    CLP_D_In_Next = MIPS_rx_data;
                    state_next = MEMA;
                    Prog_Mem_Write_Next = 1'b0;
                end
            end
            MEMA:
            begin
                Prog_Mem_Write_Next = 1'b0;
                if(rx_done_tick)
                begin
                    Byte_MEM_In_Next = MIPS_rx_data;
                    if(Prog_Mem_Address == 0)
                    begin
                        Prog_Mem_Address_Next = 0;
                    end
                    else
                    begin
                        Prog_Mem_Address_Next = Prog_Mem_Address + 1;
                    end
                    Prog_Mem_Write_Next = 1'b1; 
                    state_next = MEMB;
                end
                else
                begin
                    Prog_Mem_Write_Next = 1'b0; 
                end
            end
            MEMB:
            begin
                Prog_Mem_Write_Next = 1'b0;
                if(rx_done_tick)
                begin
                    Byte_MEM_In_Next = MIPS_rx_data;
                    Prog_Mem_Address_Next = Prog_Mem_Address + 1;
                    Prog_Mem_Write_Next = 1'b1; 
                    state_next = MEMC;
                end
                else
                begin
                    Prog_Mem_Write_Next = 1'b0; 
                end
            end
            MEMC:
            begin
                Prog_Mem_Write_Next = 1'b0;
                if(rx_done_tick)
                begin
                    Byte_MEM_In_Next = MIPS_rx_data;
                    Prog_Mem_Address_Next = Prog_Mem_Address + 1;
                    Prog_Mem_Write_Next = 1'b1; 
                    state_next = MEMD;
                end
                else
                begin
                    Prog_Mem_Write_Next = 1'b0; 
                end
            end
            MEMD:
            begin
                Prog_Mem_Write_Next = 1'b0;
                if(rx_done_tick)
                begin
                    Byte_MEM_In_Next = MIPS_rx_data;
                    Prog_Mem_Address_Next = Prog_Mem_Address + 1;
                    Prog_Mem_Write_Next = 1'b0; 
                    if(CONT == CLP - 1)
                    begin
                        state_next = EMOD;
                        Prog_Mem_Write_Next = 1'b1;
                    end
                    else
                    begin
                        state_next = MEMA;
                        CONT_Next  = CONT_Next + 1;
                        Prog_Mem_Write_Next = 1'b1;
                    end 
                end
                else
                begin
                    Prog_Mem_Write_Next = 1'b0; 
                end
            end
            EMOD:
            begin
                Prog_Mem_Write_Next = 1'b0; 
                if(rx_done_tick)
                begin
                    Mode_Op_Next = MIPS_rx_data;
                    state_next = EXEC; 
                    tx_start_next = 1'b0;
                    Debug_Stall_Next = 0;
                end
            end
            EXEC:
            begin
                if((Mode_Op == 0) && (!i_hlt))
                begin
                    state_next = EXEC;
                    tx_start_next = 1'b0;
                    Debug_Stall_Next = 0;
                end
                else
                begin
                    Debug_Stall_Next = 1;
                    state_next = REGA;
                    Data_Out_Next = table2[CONT2][7 : 0];
                    tx_start_next = 1'b1;
                end
            end
            REGA:
            begin
                Data_Out_Next = table2[CONT2][15 : 8];
                if(tx_done_tick)
                begin
                    state_next = REGB;
                    tx_start_next = 1'b1;
                end
            end
            REGB:
            begin
                Data_Out_Next = table2[CONT2][23 : 16];
                if(tx_done_tick)
                begin
                    state_next = REGC;
                end         
            end
            REGC:
            begin
                Data_Out_Next = table2[CONT2][31 : 24];
                if(tx_done_tick)
                begin
                    state_next = REGD;
                end
            end
            REGD:
            begin
                if(tx_done_tick)
                begin
                    if(CONT2 == TC_WORD - 1)
                    begin
                        if(Mode_Op == 0) // Continuo
                        begin
                            state_next = EHLT;
                            tx_start_next = 1'b0;
                        end
                        else if((Mode_Op == 1) && (!i_hlt)) // Paso
                        begin
                            tx_start_next = 1'b0;
                            CONT2_Next = 0;
                            state_next = EMOD;      
                        end
                        else
                        begin
                            state_next = EHLT;
                            tx_start_next = 1'b0;
                        end
                    end
                    else
                    begin
                        CONT2_Next = CONT2 + 1;
                        state_next = REGA;
                        tx_start_next = 1'b1;
                        Data_Out_Next = table2[CONT2 + 1][7 : 0];
                    end
                end
            end
            EHLT:
            begin
                tx_start_next = 1'b0;
                state_next = EHLT;
            end
            default
            begin
                state_next = 4'b0000;
            end
        endcase
    end

UART
#( 
    .NB_UART            (NB_UART),    // Cantidad de bits de datos
    .SB_TICK            (SB_TICK), 
    .NB_CONT            (NB_CONT),
    .NB_BRCV            (NB_BRCV),
    .M                  (M)        // Param pra Baud Rate Generator
 )
UART_1
(
    .i_clk              (i_clk), 
    .i_Reset            (i_Reset),
    .i_rx               (i_rx),
    .i_tx_data          (Data_Out),
    .i_tx_start         (tx_start_reg),
    
    .o_tx               (o_tx),
    .o_rx_done_tick     (rx_done_tick),
    .o_tx_done_tick     (tx_done_tick),
    .o_rx_data          (MIPS_rx_data)
);

endmodule
