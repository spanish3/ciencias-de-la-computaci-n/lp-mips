#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fillInstruction.h"

#define TAM 1024

void fillInstruction(char ** args, char *Inst, int j, int CLP)
{  
    char rs_base[6], rt[6], rd[6], shamt[TAM], offset_immediate[17], instr_index[27];

    char *AgroupOpcode[] = {"000000","000000","000000","000000","000000","000000","000000","000000","000000","000000"};
    char *AgroupFunction[] = {"100001","100011","100100","100101","100110","100111","101010"};
    //char *AgroupFunction[] = {"100001","100011","100100","100101","100110","100111","101010","00100","000110","000111"};

    char *BgroupOpcode[] = {"000000","000000","000000"};
    char *BgroupFunction[] = {"000000","000010","000011"};

    // Grupos C, D y E no tienen campo Function
    char *CgroupOpcode[] = {"001000","001100","001101","001110","001010","000100","000101"};

    char *DgroupOpcode[] = {"001111"};

    char *EgroupOpcode[] = {"000010","000011"};

    char *FgroupOpcode[] = {"000000"};
    char *GgroupOpcode[] = {"000000"};
    char *FgroupFunction[] = {"001000"};
    char *GgroupFunction[] = {"001001"};

    char *HgroupOpcode[] = {"100000","100001","100011","100111","100100","100101","101000","101001","101011"};

    char *IgroupOpcode[] = {"000100","000101"};

    char *JgroupOpcode[] = {"000000"};
    char *JgroupFunction[] = {"000100","000110","000111"};

    int group = 0; 
    char groupChar[5];
    char aux[6];
    char aux16[17];
    char aux26[27];

    if(args[0]!= NULL)
    {
        group = isAgroup(args[0]);  //Esta linea genera viol de seg en la segunda linea del prog
    }
    else
    {
        printf("args es null\n");
        group = -1;
    }

    if(group != -1)
    {
        removeChar(args[1],'r');
        stringToBin(args[1],aux);
        strcpy(rd,aux);
        

        removeChar(args[2],'r');
        stringToBin(args[2],aux);
        strcpy(rs_base,aux);

        removeChar(args[3],'r');
        stringToBin(args[3],aux);
        strcpy(rt,aux);

        if(j == CLP - 1)
        {
            sprintf(Inst,"%s_%s_%s_%s_00000_%s", AgroupOpcode[group], rs_base, rt, rd, AgroupFunction[group]); // Si j == last instruction no quiero el \n
        }
        else
        {
            sprintf(Inst,"%s_%s_%s_%s_00000_%s\n", AgroupOpcode[group], rs_base, rt, rd, AgroupFunction[group]);
        }
        
        //sprintf(Inst,"000000%s%s%s00000%s", rs_base, rt, rd, AgroupFunction[group]);
    }

    group = isBgroup(args[0]);
    if(group != -1)
    {
        removeChar(args[1],'r');
        stringToBin(args[1],aux);
        //strcpy(rt,aux);
        strcpy(rd,aux);

        removeChar(args[2],'r');
        stringToBin(args[2],aux);
        strcpy(rt,aux);
        //strcpy(rd,aux);

        stringToBin(args[3],aux);
        strcpy(shamt,aux);

        //sprintf(Inst,"%s_00000_%s_%s_%s_%s\n", BgroupOpcode[group], rt, rd, shamt, BgroupFunction[group]);

        if(j == CLP - 1)
        {
            sprintf(Inst,"%s_00000_%s_%s_%s_%s", BgroupOpcode[group], rt, rd, shamt, BgroupFunction[group]); // Si j == last instruction no quiero el \n
        }
        else
        {
            sprintf(Inst,"%s_00000_%s_%s_%s_%s\n", BgroupOpcode[group], rt, rd, shamt, BgroupFunction[group]);
        }
    }

    group = isCgroup(args[0]);
    if(group != -1)
    {
        removeChar(args[1],'r');
        stringToBin(args[1],aux);
        strcpy(rt,aux);

        removeChar(args[2],'r');
        stringToBin(args[2],aux);
        strcpy(rs_base,aux);

        stringToBin16(args[3],aux16);
        strcpy(offset_immediate,aux16);

        //sprintf(Inst,"%s_%s_%s_%s\n", CgroupOpcode[group], rs_base, rt, offset_immediate);

        if(j == CLP - 1)
        {
            sprintf(Inst,"%s_%s_%s_%s", CgroupOpcode[group], rs_base, rt, offset_immediate); // Si j == last instruction no quiero el \n
        }
        else
        {
            sprintf(Inst,"%s_%s_%s_%s\n", CgroupOpcode[group], rs_base, rt, offset_immediate);
        }
    }

    group = isDgroup(args[0]);
    if(group != -1)
    {
        removeChar(args[1],'r');
        stringToBin(args[1],aux);
        strcpy(rt,aux);

        stringToBin16(args[2],aux16);
        strcpy(offset_immediate,aux16);

        //sprintf(Inst,"%s_00000%s_%s\n", DgroupOpcode[group], rt, offset_immediate);

        if(j == CLP - 1)
        {
            sprintf(Inst,"%s_00000%s_%s", DgroupOpcode[group], rt, offset_immediate); // Si j == last instruction no quiero el \n
        }
        else
        {
            sprintf(Inst,"%s_00000%s_%s\n", DgroupOpcode[group], rt, offset_immediate);
        }
    }

    group = isEgroup(args[0]);
    if(group != -1)
    {
        stringToBin26(args[1],aux26);
        strcpy(instr_index,aux26);

        //sprintf(Inst,"%s_%s\n", EgroupOpcode[group],instr_index);

        if(j == CLP - 1)
        {
            sprintf(Inst,"%s_%s", EgroupOpcode[group],instr_index);// Si j == last instruction no quiero el \n
        }
        else
        {
            sprintf(Inst,"%s_%s\n", EgroupOpcode[group],instr_index);
        }
    }

    group = isFgroup(args[0]);
    if(group != -1)
    {
        removeChar(args[1],'r');
        stringToBin(args[1],aux);
        strcpy(rs_base,aux);

        //sprintf(Inst,"%s_%s_000000000000000_%s\n", FgroupOpcode[group], rs_base, FgroupFunction[group]);

        if(j == CLP - 1)
        {
            sprintf(Inst,"%s_%s_000000000000000_%s", FgroupOpcode[group], rs_base, FgroupFunction[group]);// Si j == last instruction no quiero el \n
        }
        else
        {
            sprintf(Inst,"%s_%s_000000000000000_%s\n", FgroupOpcode[group], rs_base, FgroupFunction[group]);
        }
    }

    group = isGgroup(args[0]);
    if(group != -1)
    {
        int g;
        g = 0;
        //while(args[g] != 0)
        while(args[g] != NULL)
        {
            g++;
        }

        removeChar(args[1],'r');
        //removeChar(args[2],'r');

        if(g == 3)
        {
            removeChar(args[2],'r');
            stringToBin(args[1],aux);
            strcpy(rd,aux);

            stringToBin(args[2],aux);
            strcpy(rs_base,aux);
        }
        else
        {
            stringToBin(args[1],aux);
            strcpy(rs_base,aux);

            stringToBin("31",aux);
            strcpy(rd,aux);
        }

        //sprintf(Inst,"%s_%s_00000_%s_00000_%s\n", GgroupOpcode[group], rs_base, rd, GgroupFunction[group]);

        if(j == CLP - 1)
        {
            sprintf(Inst,"%s_%s_00000_%s_00000_%s", GgroupOpcode[group], rs_base, rd, GgroupFunction[group]);// Si j == last instruction no quiero el \n
        }
        else
        {
            sprintf(Inst,"%s_%s_00000_%s_00000_%s\n", GgroupOpcode[group], rs_base, rd, GgroupFunction[group]);
        }
    }

    group = isHgroup(args[0]);
    if(group != -1)
    {
        removeChar(args[1],'r');
        stringToBin(args[1],aux);
        strcpy(rt,aux);

        stringToBin16(args[2],aux16);
        strcpy(offset_immediate,aux16);

        removeChar(args[3],'r');
        stringToBin(args[3],aux);
        strcpy(rs_base,aux);

        //sprintf(Inst,"%s_%s_%s_%s\n", HgroupOpcode[group], rs_base, rt, offset_immediate);

        if(j == CLP - 1)
        {
            sprintf(Inst,"%s_%s_%s_%s", HgroupOpcode[group], rs_base, rt, offset_immediate);// Si j == last instruction no quiero el \n
        }
        else
        {
            sprintf(Inst,"%s_%s_%s_%s\n", HgroupOpcode[group], rs_base, rt, offset_immediate);
        }
    }

    group = isIgroup(args[0]);
    if(group != -1)
    {
        removeChar(args[1],'r');
        stringToBin(args[1],aux);
        strcpy(rs_base,aux);

        removeChar(args[2],'r');
        stringToBin(args[2],aux);
        strcpy(rt,aux);

        stringToBin16(args[3],aux16);
        strcpy(offset_immediate,aux16);

        //sprintf(Inst,"%s_%s_%s_%s\n", IgroupOpcode[group], rs_base, rt, offset_immediate);

        if(j == CLP - 1)
        {
            sprintf(Inst,"%s_%s_%s_%s", IgroupOpcode[group], rs_base, rt, offset_immediate); // Si j == last instruction no quiero el \n
        }
        else
        {
            sprintf(Inst,"%s_%s_%s_%s\n", IgroupOpcode[group], rs_base, rt, offset_immediate); 
        }
    }

    group = isJgroup(args[0]);
    if(group != -1)
    {
        //printf("Es grupo j\n");
        removeChar(args[1],'r');
        stringToBin(args[1],aux);
        strcpy(rd,aux);
        

        removeChar(args[2],'r');
        stringToBin(args[2],aux);
        strcpy(rt,aux);
        //strcpy(rs_base,aux);

        removeChar(args[3],'r');
        stringToBin(args[3],aux);
        strcpy(rs_base,aux);
        //strcpy(rt,aux);

        if(j == CLP - 1)
        {
            sprintf(Inst,"000000_%s_%s_%s_00000_%s", rs_base, rt, rd, JgroupFunction[group]); // Si j == last instruction no quiero el \n
            //printf(Inst,"%s_%s_%s_%s_00000_%s", JgroupOpcode[group], rs_base, rt, rd, JgroupFunction[group]);
        }
        else
        {
            sprintf(Inst,"000000_%s_%s_%s_00000_%s\n", rs_base, rt, rd, JgroupFunction[group]);
        }
        
        //sprintf(Inst,"000000%s%s%s00000%s", rs_base, rt, rd, AgroupFunction[group]);
    }

    group = isNOP(args[0]);
    if(group != -1)
    {
        //sprintf(Inst,"111110_00000_00000_00000_00000_000000\n");
        if(j == CLP - 1)
        {
            sprintf(Inst,"111110_00000_00000_00000_00000_000000"); // Si j == last instruction no quiero el \n
        }
        else
        {
            sprintf(Inst,"111110_00000_00000_00000_00000_000000\n");
        }
    }

    group = isHLT(args[0]);
    if(group != -1)
    {
        //sprintf(Inst,"111111_00000_00000_00000_00000_000000\n");
        if(j == CLP - 1)
        {
            sprintf(Inst,"111111_00000_00000_00000_00000_000000"); // Si j == last instruction no quiero el \n
        }
        else
        {
            sprintf(Inst,"111111_00000_00000_00000_00000_000000\n");
        }
    }
    
}

int isAgroup(char *mnem)
{
    //char *Agroup[] = {"addu","subu","and","or","xor","nor","slr","sllv","srlv","srav",NULL};
    char *Agroup[] = {"addu","subu","and","or","xor","nor","slt",NULL};
    int i ;
    i = 0;
    while(Agroup[i] != NULL)
    {
        if(!strcmp(mnem,Agroup[i])) // Esta linea genera el segmentation fault
        {
            return i;
        }
        i++;
    }
    return -1;
}

int isBgroup(char *mnem)
{
    char *Instrucciones[] = {"sll","srl","sra",NULL};
    int i = 0;
    while(Instrucciones[i] != NULL)
    {
        if(!strcmp(mnem,Instrucciones[i]))
        {
            return i;
        }
        i++;
    }
    return -1;
}

int isCgroup(char *mnem)
{
    char *Instrucciones[] = {"addi","andi","ori","xori","slti",NULL};
    int i = 0;
    while(Instrucciones[i] != NULL)
    {
        if(!strcmp(mnem,Instrucciones[i]))
        {
            return i;
        }
        i++;
    }
    return -1;
}

int isDgroup(char *mnem)
{
    char *Instrucciones = "lui";

    if(!strcmp(mnem,Instrucciones))
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

int isEgroup(char *mnem)
{
    char *Instrucciones[] = {"j","jal",NULL};
    
    int i = 0;
    while(Instrucciones[i] != NULL)
    {
        if(!strcmp(mnem,Instrucciones[i]))
        {
            return i;
        }
        i++;
    }
    return -1;
}

int isFgroup(char *mnem)
{
    char *Instrucciones = "jr";

    if(!strcmp(mnem,Instrucciones))
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

int isGgroup(char *mnem)
{
    char *Instrucciones = "jalr";

    if(!strcmp(mnem,Instrucciones))
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

int isHgroup(char *mnem)
{
    char *Instrucciones[] = {"lb","lh","lw","lwu","lbu","lhu","sb","sh","sw",NULL};
    int i = 0;
    while(Instrucciones[i] != NULL)
    {
        if(!strcmp(mnem,Instrucciones[i]))
        {
            return i;
        }
        i++;
    }
    return -1;
}

int isIgroup(char *mnem)
{
    char *Instrucciones[] = {"beq","bne",NULL};
    int i = 0;
    while(Instrucciones[i] != NULL)
    {
        if(!strcmp(mnem,Instrucciones[i]))
        {
            return i;
        }
        i++;
    }
    return -1;
}

int isJgroup(char *mnem)
{
    char *Instrucciones[] = {"sllv","srlv","srav",NULL};
    int i = 0;
    while(Instrucciones[i] != NULL)
    {
        if(!strcmp(mnem,Instrucciones[i]))
        {
            return i;
        }
        i++;
    }
    return -1;
}

int isNOP(char *mnem)
{
    char *Instrucciones = "nop";

    if(!strcmp(mnem,Instrucciones))
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

int isHLT(char *mnem)
{
    char *Instrucciones = "hlt";

    if(!strcmp(mnem,Instrucciones))
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

void stringToBin(char *string, char *binRep)
{
    int num;
    int binNum[5] = {0,0,0,0,0};

    num = (int)strtol(string,NULL,10);
    //printf("Decimal: %d\n",num);

    for(int i=4; num>0; i--)    
    {    
        binNum[i]=num%2;    
        num=num/2;    
    }
 
    sprintf(binRep,"%d%d%d%d%d", binNum[0], binNum[1], binNum[2], binNum[3], binNum[4]); 
}

void stringToBin16(char *string, char *binRep)
{
    int num;
    int binNum[17] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

    num = (int)strtol(string,NULL,10);
    //printf("Decimal: %d\n",num);

    for(int i=15; num>0; i--)    
    {    
        binNum[i]=num%2;    
        num=num/2;    
    }
 
    sprintf(binRep,"%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d", binNum[0], binNum[1], binNum[2], binNum[3], binNum[4],
    binNum[5],binNum[6],binNum[7],binNum[8],binNum[9],binNum[10],binNum[11],binNum[12],binNum[13],binNum[14],binNum[15]); 
}

void stringToBin26(char *string, char *binRep)
{
    int num;
    int binNum[27] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

    num = (int)strtol(string,NULL,10);
    //printf("Decimal: %d\n",num);

    for(int i=25; num>0; i--)    
    {    
        binNum[i]=num%2;    
        num=num/2;    
    }
 
    sprintf(binRep,"%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d", binNum[0], binNum[1], binNum[2], binNum[3], binNum[4],
    binNum[5],binNum[6],binNum[7],binNum[8],binNum[9],binNum[10],binNum[11],binNum[12],binNum[13],binNum[14],binNum[15],
    binNum[16], binNum[17], binNum[18], binNum[19], binNum[20],binNum[21],binNum[22],binNum[23],binNum[24],binNum[25]); 
}

void stringToBin32(char *string, char *binRep)
{
    int num;
    int binNum[33] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

    num = (int)strtol(string,NULL,10);
    //printf("Decimal: %d\n",num);

    for(int i=31; num>0; i--)    
    {    
        binNum[i]=num%2;    
        num=num/2;    
    }
 
    sprintf(binRep,"%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d", binNum[0], binNum[1], binNum[2], binNum[3], binNum[4],
    binNum[5],binNum[6],binNum[7],binNum[8],binNum[9],binNum[10],binNum[11],binNum[12],binNum[13],binNum[14],binNum[15],
    binNum[16], binNum[17], binNum[18], binNum[19], binNum[20],binNum[21],binNum[22],binNum[23],binNum[24],binNum[25],
    binNum[26],binNum[27],binNum[28],binNum[29],binNum[30],binNum[31]); 
}

void removeChar(char *string, char toRemove) // Esto no funciona bien
{
  int i, j;
  int len = strlen(string);
  for(i = 0; i < len; i++)
  {
    if(string[i] == toRemove)
    {
      for(j = i; j < len; j++)
      {
        string[j] = string [j + 1];
      }
      len--;
      i--;
    }
  }
}