`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:15:25
// Design Name: 
// Module Name: UART_TX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module UART_TX
#(
    parameter   NB_UART = 8,    // Cantidad de bits de datos
    parameter   SB_TICK = 16,   // Cantidad de ticks necesarios para los bits de stop
    parameter   NB_STAT = 2,    // Cantidad de bits para representar los estdos    
    parameter   NB_CONT = 4,
    parameter   NB_BRCV = 3
 )
 (
    // Entradas
    input   wire                                i_clk,
    input   wire                                i_Reset,        
    input   wire                                i_tx_start,           
    input   wire                                s_tick,         // Se?al de Baud Rate Gen
    input   wire    signed  [NB_UART - 1 : 0]   i_din,
    
    // Salidas
    output  reg                                 o_tx_done_tick,
    output  wire                                o_tx
    
 );
 
    // Estados
    localparam              [NB_STAT - 1 : 0]   idle  = 2'b00;
    localparam              [NB_STAT - 1 : 0]   start = 2'b01;   // s_reg cuenta hasta 7
    localparam              [NB_STAT - 1 : 0]   data  = 2'b10;   // s_reg cuenta hasta 15
    localparam              [NB_STAT - 1 : 0]   stop  = 2'b11;   // s_reg cuenta hasta SB_TICK
    
    // Interno
    reg                     [NB_STAT - 1 : 0]   state_reg;      // Estado actual y estado siguiente
    reg                     [NB_STAT - 1 : 0]   state_next;     // s_reg cuenta la cantidad de ticks que ocurrieron
    reg                     [NB_CONT - 1 : 0]   s_reg;          
    reg                     [NB_CONT - 1 : 0]   s_next;
    reg                     [NB_BRCV - 1 : 0]   n_reg;          // n_reg cuenta la cantidad de datos enviados
    reg                     [NB_BRCV - 1 : 0]   n_next;
    reg     signed          [NB_UART - 1 : 0]   b_reg;          
    reg     signed          [NB_UART - 1 : 0]   b_next;
    reg                                         tx_reg;
    reg                                         tx_next;
    
    // Estados - Secuencial
    // Parte secuendial: Estados
always @(posedge i_clk, posedge i_Reset)
    if(i_Reset)   // Ante un i_Reset (o inicialmente) todos los contadores se encuentran en cero y el estado actual es idle.
        begin
            state_reg <= idle;
            s_reg  <= 0;
            n_reg  <= 0;
            b_reg  <= 0;
            tx_reg <= 1'b1; 
        end
    else
        begin
            state_reg <= state_next;
            s_reg  <= s_next;
            n_reg  <= n_next;
            b_reg  <= b_next;
            tx_reg <= tx_next;
        end
      
// Parte combinacional:
always @(*)
    begin
        state_next = state_reg;
        o_tx_done_tick = 1'b0;
        s_next  = s_reg;
        n_next  = n_reg;
        b_next  = b_reg;
        tx_next = tx_reg;
        case (state_reg)
            idle:
            begin
                tx_next = 1'b1;
                if(i_tx_start)
                begin
                    state_next = start;
                    s_next = 0;
                    b_next = i_din;
                end
            end                
            start:
            begin
                tx_next = 1'b0;
                if(s_tick)
                begin
                    if(s_reg == 15)
                    begin
                        state_next = data;
                        s_next = 0;
                        n_next = 0;
                    end
                    else
                    begin
                        s_next = s_reg + 1; 
                    end
                end                                              
            end
            data:
            begin
                tx_next = b_reg[0];
                if(s_tick)
                begin
                    if(s_reg == 15)
                    begin
                        s_next = 0;
                        b_next = b_reg >> 1;
                        if(n_reg == (NB_UART - 1))
                        begin
                            state_next = stop;
                        end
                        else
                        begin
                            n_next = n_reg + 1; 
                        end                                                               
                    end
                    else
                    begin
                        s_next = s_reg +1; 
                    end                       
                end
            end
            stop:
            begin
                tx_next = 1'b1;
                if(s_tick)
                begin
                    if(s_reg == (SB_TICK - 1))
                    begin
                        state_next   = idle;
                        o_tx_done_tick = 1'b1;
                    end
                    else
                    begin
                        s_next = s_reg + 1;
                    end
                end
            end                        
        endcase                             
    end
  
// Salida
assign o_tx = tx_reg;

endmodule
