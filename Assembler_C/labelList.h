#ifndef LABELLIST_H    // Comienzo del ifdef guard
#define LABELLIST_H

    struct labelNode 
    {
    char labelText[256];
    int dir;
    struct labelNode *next;
    };
      
    void addLabel(struct labelNode * head, char *text, int labelDir);

    int labelExists(struct labelNode * head, char *text);

    void printLabelList(struct labelNode * head);

    int LabelListSize(struct labelNode * head);

    void freeList(struct labelNode * head);

    void *getLabelNumber(struct labelNode * head, char *label, char *dest);

    int getIntLabelNumber(struct labelNode * head, char *label);

#endif // Fin del ifdef guard