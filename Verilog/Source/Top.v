`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:16:42
// Design Name: 
// Module Name: Top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Top
#(
    parameter   NB_DATA = 32,
    parameter   NB_UART = 8,    // Cantidad de bits de datos
    parameter   SB_TICK = 16,
    parameter   NB_CONT = 4,
    parameter   NB_BRCV = 3,
    parameter   M = 14,        // Param pra Baud Rate Generator
    parameter   W = 8,          // # buffer de W bits
    parameter   OP_BITS = 6,    // Bits de operaciones
    parameter   NB_STAT = 4,    // Cantidad de bits para representar estados
    parameter   NB_SEND = 384, // Cantidad de bits a enviar (tabla)
    parameter   TC_WORD = 12,    // Cantidad de palabras de 32 bits en la tabla
    parameter   NB_DIR_PC   = 32,
    parameter   NB_ALUOP    = 4,
    parameter   NB_SHAMT    = 5,
    parameter   NB_OPCODE   = 6,
    parameter   NB_FUNC     = 6,
    parameter   NB_RDIR     = 5,     // Bits de direccionmaiento de registros
    parameter   NB_MUX4     = 2,
    parameter   NB_ALLR     = 160,  // Cantidad bits de registros a enviar
    parameter   NB_DMEM     = 160    // Cantidad de bits de memoria de datos a enviar
 )
(
    input   wire                          i_clk,                 
    input   wire                          i_Reset,               
    input   wire                          i_Reset_clk,  
    input   wire                          i_rx,
   
    output  wire                          o_tx,
    output  wire                          o_locked    
);
 
    wire                                clk_out_1;
    wire                                hlt;
    wire        [NB_SEND  - 1 : 0]      r_table;
    wire                                Prog_Mem_Write;
    wire        [NB_DATA  - 1 : 0]      Prog_Mem_Address;
    wire        [NB_UART  - 1 : 0]      Write_Data;
    wire                                Debug_Stall;
   
clk_wiz_0 tpf_clk_wiz
( 
    .clk_out1(clk_out_1),            
    .reset(i_Reset_clk), 
    .locked(o_locked),
    .clk_in1(i_clk)
);

Debug_Unit
#(
    .NB_DATA                (NB_DATA),
    .NB_UART                (NB_UART),     // Cantidad de bits de datos
    .W                      (W),           // # buffer de W bits
    .OP_BITS                (OP_BITS),     // Bits de operaciones
    .NB_STAT                (NB_STAT),     // Cantidad de bits para representar estados
    .NB_SEND                (NB_SEND),     // Cantidad de bits a enviar (tabla)
    .TC_WORD                (TC_WORD)      // Cantidad de palabras de 32 bits en la tabla
)
u_Debug_Unit_1
(
    .i_clk                  (clk_out_1),
    .i_Reset                (i_Reset),
   
    .i_rx                   (i_rx),        

    .i_hlt                  (hlt),
    .i_table                (r_table),  
   
    // Salidas Program Memory
    .o_Prog_Mem_Address     (Prog_Mem_Address),
    .o_Prog_Mem_Byte        (Write_Data[7 : 0]),
    .o_Prog_Mem_Write       (Prog_Mem_Write),
   
    // Salidas MIPS
    .o_Debug_Stall          (Debug_Stall),
   
    // Salidas Tx
    .o_tx                   (o_tx)    
);

MIPS
#(
    .NB_DATA                (NB_DATA),
    .NB_UART                (NB_UART),
    .NB_DIR_PC              (NB_DIR_PC),
    .NB_ALUOP               (NB_ALUOP),
    .NB_SHAMT               (NB_SHAMT),
    .NB_OPCODE              (NB_OPCODE),
    .NB_FUNC                (NB_FUNC),
    .NB_RDIR                (NB_RDIR),     // Bits de direccionmaiento de registros
    .NB_MUX4                (NB_MUX4),
    .NB_SEND                (NB_SEND),     // Cantidad de bits a enviar (tabla)
    .NB_ALLR                (NB_ALLR),     // Cantidad bits de registros a enviar
    .NB_DMEM                (NB_DMEM)      // Cantidad de bits de memoria de datos a enviar
 )
u_MIPS_1
(
    .i_clk                  (clk_out_1),
    .i_Reset                (i_Reset),                // Reset
    .i_Stall                (Debug_Stall),  
    .i_Prog_Write_Address   (Prog_Mem_Address),
    .i_Prog_MEM_Data        (Write_Data),
    .i_Prog_MEM_Write       (Prog_Mem_Write),
    // Salidas
    .o_HLT                  (hlt),
    .o_table                (r_table)
 );
   
endmodule
