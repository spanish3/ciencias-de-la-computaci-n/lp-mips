# Arquitectura de Computadoras - MIPS Segmentado

## Consigna y Requerimientos

- **Implementación del Procesador MIPS Segmentado**: El procesador debe ser implementado en las siguientes etapas:
  - IF (Instruction Fetch): Búsqueda de la instrucción en la memoria de programa.
  - ID (Instruction Decode): Decodificación de la instrucción y lectura de registros.
  - EX (Execute): Ejecución de la instrucción.
  - MEM (Memory Access): Lectura o escritura desde/hacia la memoria de datos.
  - WB (Write Back): Escritura de resultados en los registros.
  
- **Soporte para Riesgos**: El procesador debe tener soporte para los siguientes tipos de riesgos:
  - Estructurales: Se producen cuando dos instrucciones tratan de usar el mismo recurso en el mismo ciclo.
  - De Datos: Se intenta utilizar un dato antes de que esté preparado. Mantenimiento del orden estricto de lecturas y escrituras.
  - De Control: Intentar tomar una decisión sobre una condición todavía no evaluada. Para dar soporte a estos riesgos se debe implementar una unidad de cortocircuitos y una unidad de detección de riesgos.

- **Instrucciones a Implementar**: Se deben implementar las siguientes instrucciones:
  - R-Type: SLL, SRL, SRA, SLLV, SRLV, SRAV, ADDU, SUBU, AND, OR, XOR, NOR, SLT.
  - I-Type: LB, LH, LW, LWU, LBU, LHU, SB, SH, SW, ADDI, ANDI, ORI, XORI, LUI, SLTI, BEQ, BNE, J, JAL.
  - J-Type: JR, JALR.
  
- **Ensamblado del Programa y Carga en la Memoria**: El programa a ejecutar debe ser cargado en la memoria de programa mediante un archivo ensamblado. Se deben realizar las siguientes tareas:
  - Implementación de un programa ensamblador que convierte código assembler de MIPS a código de instrucción.
  - Transmisión de ese programa mediante interfaz UART antes de comenzar a ejecutar.

- **Debug Unit**: Se debe simular una unidad de Debug que envíe información hacia y desde el procesador mediante UART. La información a enviar a través de la UART incluye:
  - Contenido de los 32 registros.
  - PC.
  - Contenido de la memoria de datos usada.
  - Cantidad de ciclos de clock desde el inicio.

- **Modos de Operación**: El procesador debe permitir dos modos de operación:
  - Continuo: Se envía un comando a la FPGA por la UART y esta inicia la ejecución del programa hasta llegar al final del mismo (Instrucción HALT). Llegado ese punto se muestran todos los valores indicados en pantalla.
  - Paso a Paso: Enviando un comando por la UART se ejecuta un ciclo de Clock. Se debe mostrar a cada paso los valores indicados.

- **Validación del Desarrollo**: Presentar el trabajo simulado y validar el desarrollo por medio de Test Bench.

- **Simulación Post-Síntesis**: La simulación debe ser post-síntesis e incluir información de timing. Se deben seguir los siguientes pasos:
  - El clock del sistema debe crearse usando el ip-core correspondiente.
  - Mostrar reporte de timing, con máxima frecuencia de clock soportada.


## Clonar Repositorio

```bash
cd existing_repo
git remote add origin https://gitlab.com/spanish3/ciencias-de-la-computaci-n/lp-mips.git
git branch -M main
git pull origin main
```

## Preparación de la Simulación

Antes de simular el procesador, asegúrese de realizar los siguientes pasos:

1. **Importar Archivos de Simulación**: Los archivos `.mem` deben ser importados como fuentes de simulación. Estos archivos contienen el programa en lenguaje máquina, la cantidad de líneas del programa y la salida final del procesador. Los archivos son:
   - `Code.mem`: Contiene el programa en lenguaje máquina, enviado al procesador línea por línea mediante UART.
   - `CLP.mem`: Contiene la cantidad de líneas que posee el programa, utilizado por la máquina de estados que controla el módulo UART.
   - `Output.mem`: Contiene el valor final de PC, la cantidad de clocks utilizados, los valores de los 5 primeros registros y las 5 primeras posiciones de la memoria de datos.

2. **Generación de Módulo de Clocking**: Es necesario generar el módulo `clk_wiz_0` utilizando Clocking Wizard.

## Módulos del Proyecto

El proyecto consta de los siguientes módulos:

- **Top**: Es el resultado principal del proyecto, el cual se cargaría en la FPGA.
- **Debug_Interface**: Contraparte del módulo Debug_Unit contenido en Top. Facilita la comunicación entre el programa y los datos una vez que el programa ha sido ejecutado.
- **Isolated_MIPS**: Versión del MIPS sin los módulos para cargar el programa ni controlar el modo de funcionamiento. Utilizado para pruebas en simulaciones más cortas.
- **Non_CLK_Top** y **Non_CLK_Debug_Interface**: Versiones de Top y Debug_Interface sin el módulo generado por el Clocking Wizard. Utilizado para simular el sistema completo en modo puramente comportamental.

## Cambio de Modo de Funcionamiento

Para cambiar el modo de funcionamiento, siga estos pasos:

1. Modifique la variable `Mode_Op` en `Non_CLK_Debug_Testbench`.
   - `Mode_Op = 0`: Modo Continuo.
   - `Mode_OP = 1`: Modo Paso a Paso.

### Para explicaciones sobre funcionamiento del sistema completo, resultados de simulaciones y análisis de timing, acudir al informe.